<?php /* Smarty version 2.6.11, created on 2015-06-15 02:36:01
         compiled from themes/Corporate_Style/tpls/_thinkFooter.tpl */ ?>

<div id="thinkFooter">
	<?php 
	    global $current_user, $current_language, $app_strings, $app_list_strings, $mod_strings;
	    $this->assign("thinkFooter", $app_strings);
	 ?>
	<a href="javascript:void(0)" onclick="SUGAR.util.top();" id="expandTopLink">Top</a>
		<ul class="thinkFooterMenu">
		<li class="item-152"><a href="http://www.think7Strategy.com" rel="nofollow" target="_blank"><?php echo $this->_tpl_vars['thinkFooter']['LBL_FOOTERMENU_WEBSITE']; ?>
</a></li>
		<li class="item-152"><a href="http://www.ithinkopen.com/" rel="nofollow" target="_blank"><?php echo $this->_tpl_vars['thinkFooter']['LBL_FOOTERMENU_SUPPORT_COMMUNITY']; ?>
</a></li>
		<li class="item-152">
			<a href="http://www.sugarcrm.com/" rel="nofollow" target="_blank"><?php echo $this->_tpl_vars['thinkFooter']['LBL_FOOTERMENU_POWERED_BY']; ?>
</a>
			<div class="infoExpand active">
				<div class="copyrightInfo"><?php echo $this->_tpl_vars['COPYRIGHT']; ?>
</div>
			</div>
		</li>
		<?php if ($this->_tpl_vars['AUTHENTICATED']): ?>
		<li class="item-152"><a href="index.php?module=Calendar&amp;action=index"><?php echo $this->_tpl_vars['thinkFooter']['LBL_FOOTERMENU_CALENDAR']; ?>
</a></li>
		<li class="item-152"><a href="index.php?module=Emails&amp;action=index"><?php echo $this->_tpl_vars['thinkFooter']['LBL_FOOTERMENU_EMAILS']; ?>
</a></li>
		<li class="item-152"><a href="index.php?module=Meetings&amp;action=index"><?php echo $this->_tpl_vars['thinkFooter']['LBL_FOOTERMENU_MEETINGS']; ?>
</a></li>
		<li class="item-152"><a href="index.php?module=Calls&amp;action=index"><?php echo $this->_tpl_vars['thinkFooter']['LBL_FOOTERMENU_CALLS']; ?>
</a></li>
		<?php endif; ?>
	</ul>
	<div id="bottomWidgets">
		<?php if ($this->_tpl_vars['AUTHENTICATED']): ?>
		<div id="bottomLinks">
			<?php echo $this->_tpl_vars['BOTTOMLINKS']; ?>

		</div>
		<?php endif; ?>
		<div id="responseTime">
			<?php echo $this->_tpl_vars['STATISTICS']; ?>

		</div>
	</div>
	<p>
		Theme Design &copy; <a href="http://cosl.ithinkopen.com/sugarcrm" rel="nofollow" target="_blank" title="SugarCRM Theme">COSL SugarCRM Theme</a>
	</p>
	<div id="expandStyleArea" class="thinkFooterColorMenu"> 
		<a href="#" id="expandColor1">Style I</a> 
		<a href="#" id="expandColor2">Style II</a> 
		<a href="#" id="expandColor3">Style III</a> 
	</div>
</div>
<div id="footer" class="none"></div>
<div class="clear"></div>