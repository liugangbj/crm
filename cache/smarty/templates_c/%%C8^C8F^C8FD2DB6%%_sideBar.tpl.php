<?php /* Smarty version 2.6.11, created on 2015-06-15 02:36:00
         compiled from themes/Corporate_Style/tpls/_sideBar.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'sugar_link', 'themes/Corporate_Style/tpls/_sideBar.tpl', 45, false),)), $this); ?>

<div id="sideBar" class="<?php if ($this->_tpl_vars['USE_GROUP_TABS']): ?>isGroupedModule<?php endif; ?>">
    <div id="leftColumn">
            
                                    <?php if (count ( $this->_tpl_vars['SHORTCUT_MENU'] ) > 0): ?>
            <div id="shortcuts" class="leftList">
                <h3><span><?php echo $this->_tpl_vars['APP']['LBL_SHORTCUTS']; ?>
</span></h3>
                <ul id="ul_shortcuts">
                <?php $_from = $this->_tpl_vars['SHORTCUT_MENU']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                <li style="white-space:nowrap;">
                    <?php if ($this->_tpl_vars['item']['URL'] == "-"): ?>
                      <a></a><span>&nbsp;</span>
                    <?php else: ?>
                      <a href="<?php echo $this->_tpl_vars['item']['URL']; ?>
"><span class="y"><?php echo $this->_tpl_vars['item']['IMAGE']; ?>
</span><span><?php echo $this->_tpl_vars['item']['LABEL']; ?>
</span></a>
                    <?php endif; ?>
                </li>
                <?php endforeach; endif; unset($_from); ?>
                </ul>
            </div>
            <?php endif; ?>
            
                        <div id="lastView" class="leftList">
                <h3><span><?php echo $this->_tpl_vars['APP']['LBL_LAST_VIEWED']; ?>
</span></h3>
                <ul id="ul_lastview">
                <?php $_from = $this->_tpl_vars['recentRecords']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['lastViewed'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['lastViewed']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['item']):
        $this->_foreach['lastViewed']['iteration']++;
?>
                <li style="white-space:nowrap;">
                    <a title="<?php echo $this->_tpl_vars['item']['item_summary']; ?>
"
                        accessKey="<?php echo $this->_foreach['lastViewed']['iteration']; ?>
"
                        href="<?php echo smarty_function_sugar_link(array('module' => $this->_tpl_vars['item']['module_name'],'action' => 'DetailView','record' => $this->_tpl_vars['item']['item_id'],'link_only' => 1), $this);?>
">
                        <span class="y"><?php echo $this->_tpl_vars['item']['image']; ?>
</span><span><?php echo $this->_tpl_vars['item']['item_summary_short']; ?>
</span>
                    </a>
                </li>
                <?php endforeach; else: ?>
                <li style="white-space:nowrap;">
                    <?php echo $this->_tpl_vars['APP']['NTC_NO_ITEMS_DISPLAY']; ?>

                </li>
                <?php endif; unset($_from); ?>
                </ul>
            </div>
            
                                    
    </div>
</div>