<?php /* Smarty version 2.6.11, created on 2015-06-15 02:36:00
         compiled from themes/Corporate_Style/tpls/_head.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'sugar_getjspath', 'themes/Corporate_Style/tpls/_head.tpl', 25, false),array('function', 'sugar_getimagepath', 'themes/Corporate_Style/tpls/_head.tpl', 52, false),)), $this); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html <?php echo $this->_tpl_vars['langHeader']; ?>
>
<head>
<link rel="SHORTCUT ICON" href="<?php echo $this->_tpl_vars['FAVICON_URL']; ?>
">
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $this->_tpl_vars['APP']['LBL_CHARSET']; ?>
">
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<title><?php echo $this->_tpl_vars['APP']['LBL_BROWSER_TITLE']; ?>
</title>
<?php echo $this->_tpl_vars['SUGAR_CSS']; ?>


<?php if ($_REQUEST['module'] == 'ModuleBuilder'): ?>
<link rel="stylesheet" type="text/css" href="<?php echo smarty_function_sugar_getjspath(array('file' => "themes/Corporate_Style/css/extendMB_style.css"), $this);?>
" />
<?php endif; ?>
<?php 
    //global $sugar_config;
    //$this->assign('sugarConfigNew', $sugar_config);
    //global $sugar_version;
    //$this->assign('sugarVersionNew', $sugar_version);
    //global $current_user;
    //$this->assign('currentUserNew', $current_user);
    //global $sugar_flavor;
    //$this->assign('sugarFlavorNew', $sugar_flavor);
    global $current_language;
    $this->assign('currentLanguageNew', $current_language);
 ?>
<?php if ($this->_tpl_vars['currentLanguageNew'] == 'zh_CN'): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo smarty_function_sugar_getjspath(array('file' => "themes/Corporate_Style/css/extendLangCN_style.css"), $this);?>
" />
<?php endif; ?>

<?php echo $this->_tpl_vars['SUGAR_JS']; ?>

<?php echo '
<script type="text/javascript">
<!--
SUGAR.themes.theme_name = \'';  echo $this->_tpl_vars['THEME'];  echo '\';
SUGAR.themes.theme_ie6compat = ';  echo $this->_tpl_vars['THEME_IE6COMPAT'];  echo ';
SUGAR.themes.hide_image = \'';  echo smarty_function_sugar_getimagepath(array('file' => "hide.gif"), $this); echo '\';
SUGAR.themes.show_image = \'';  echo smarty_function_sugar_getimagepath(array('file' => "show.gif"), $this); echo '\';
SUGAR.themes.loading_image = \'';  echo smarty_function_sugar_getimagepath(array('file' => "img_loading.gif"), $this); echo '\';
SUGAR.themes.allThemes = eval(';  echo $this->_tpl_vars['allThemes'];  echo ');
if ( YAHOO.env.ua )
    UA = YAHOO.env.ua;
-->
</script>
'; ?>

<script type="text/javascript" src='<?php echo smarty_function_sugar_getjspath(array('file' => "cache/include/javascript/sugar_field_grp.js"), $this);?>
'></script>
</head>