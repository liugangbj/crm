<?php /* Smarty version 2.6.11, created on 2015-06-15 02:36:00
         compiled from themes/Corporate_Style/tpls/_globalLinks.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'sugar_getimage', 'themes/Corporate_Style/tpls/_globalLinks.tpl', 35, false),)), $this); ?>

<div id="globalLinks">
<ul id="globalLinksExtraMenu">
    <li id="welcome">
        <a id="welcome_link" href="index.php?module=Users&action=EditView&record=<?php echo $this->_tpl_vars['CURRENT_USER_ID']; ?>
"><?php echo $this->_tpl_vars['CURRENT_USER']; ?>
</a>
    <ul class="cssmenu">
        <?php 
            global $sugar_flavor;
            $this->assign('sugarFlavorNew', $sugar_flavor);
         ?>
        <?php if ($this->_tpl_vars['sugarFlavorNew'] == 'CE'): ?>
            <li><a id="Profile_link" href="index.php?module=Users&action=EditView&record=<?php echo $this->_tpl_vars['CURRENT_USER_ID']; ?>
"><?php echo $this->_tpl_vars['APP']['LBL_PROFILE']; ?>
</a> </li>
        <?php endif; ?>
        <?php $_from = $this->_tpl_vars['GCLS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['gcl'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['gcl']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['gcl_key'] => $this->_tpl_vars['GCL']):
        $this->_foreach['gcl']['iteration']++;
?>
            <?php $_from = $this->_tpl_vars['GCL']['SUBMENU']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['gcl_submenu'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['gcl_submenu']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['gcl_submenu_key'] => $this->_tpl_vars['GCL_SUBMENU']):
        $this->_foreach['gcl_submenu']['iteration']++;
?>
                <a id="<?php echo $this->_tpl_vars['gcl_submenu_key']; ?>
_link" href="<?php echo $this->_tpl_vars['GCL_SUBMENU']['URL']; ?>
"<?php if (! empty ( $this->_tpl_vars['GCL_SUBMENU']['ONCLICK'] )): ?> onclick="<?php echo $this->_tpl_vars['GCL_SUBMENU']['ONCLICK']; ?>
"<?php endif; ?>><?php echo $this->_tpl_vars['GCL_SUBMENU']['LABEL']; ?>
</a>
            <?php endforeach; endif; unset($_from); ?>
            <li>
                <a id="<?php echo $this->_tpl_vars['gcl_key']; ?>
_link" href="<?php echo $this->_tpl_vars['GCL']['URL']; ?>
"<?php if (! empty ( $this->_tpl_vars['GCL']['ONCLICK'] )): ?> onclick="<?php echo $this->_tpl_vars['GCL']['ONCLICK']; ?>
"<?php endif; ?>><?php echo $this->_tpl_vars['GCL']['LABEL']; ?>
</a>
                <?php $_from = $this->_tpl_vars['GCL']['SUBMENU']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['gcl_submenu'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['gcl_submenu']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['gcl_submenu_key'] => $this->_tpl_vars['GCL_SUBMENU']):
        $this->_foreach['gcl_submenu']['iteration']++;
?>
                <?php if (($this->_foreach['gcl_submenu']['iteration'] <= 1)): ?>
                <?php echo smarty_function_sugar_getimage(array('name' => 'menuarrow','ext' => ".gif",'alt' => "",'other_attributes' => ''), $this);?>
<br />
                <ul class="cssmenu">
                <?php endif; ?>
                <li><a id="<?php echo $this->_tpl_vars['gcl_submenu_key']; ?>
_link" href="<?php echo $this->_tpl_vars['GCL_SUBMENU']['URL']; ?>
"<?php if (! empty ( $this->_tpl_vars['GCL_SUBMENU']['ONCLICK'] )): ?> onclick="<?php echo $this->_tpl_vars['GCL_SUBMENU']['ONCLICK']; ?>
"<?php endif; ?>><?php echo $this->_tpl_vars['GCL_SUBMENU']['LABEL']; ?>
</a></li>
                <?php if (($this->_foreach['gcl_submenu']['iteration'] == $this->_foreach['gcl_submenu']['total'])): ?>
                </ul>
                <?php endif; ?>
                <?php endforeach; endif; unset($_from); ?>
            </li>
        <?php endforeach; endif; unset($_from); ?>
        <?php if (! empty ( $this->_tpl_vars['LOGOUT_LINK'] ) && ! empty ( $this->_tpl_vars['LOGOUT_LABEL'] )): ?>
            <li><a id="logout_link" href='<?php echo $this->_tpl_vars['LOGOUT_LINK']; ?>
' class='utilsLink'><?php echo $this->_tpl_vars['LOGOUT_LABEL']; ?>
</a> </li>
        <?php endif; ?>
    </ul>
    </li>
</ul>
</div>