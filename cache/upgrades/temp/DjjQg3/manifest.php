<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * Corporate Style theme for SugarCRM
 * v2.6.3 (Build20131019AM2)
 * 
 * Community: http://www.ithinkopen.com
 * SugarCRM Chinese Forum: http://www.ithinkopen.com/sugarcrm
 * SugarForge: http://www.sugarforge.org/projects/corporatestyle
 * 
 * Published on COSL SugarCRM OpenSource Library http://cosl.ithinkopen.com/sugarcrm
 * "Powered by think7Strategy Org.".
 ********************************************************************************/


$manifest = array(
	'acceptable_sugar_versions' => array(
		'exact_matches' => array(/*'6.5.15'*/),
		'regex_matches' => array('6\.*\.*'),
	),
	'acceptable_sugar_flavors' => array('CE', 'PRO', 'CORP', 'ENT',),
	'author' => '泽战 | Start7',
	'description' => 'A enterprise style theme for SugarCRM',
	'icon' => '',
	'is_uninstallable' => true,
	'name' => 'Corporate Style theme',
	'published_date' => '10-19-2013',
	'type' => 'module',
	'version' => 'v2.6.3',
	);
          
$installdefs = array(
	'id' => 'CorporateStyle',
	/* SugarBeans */
	/*'beans' => array (
		array (
			'module' => '<ModuleName>',
			'class' => '<ModuleName $class Name>',
			'path' => 'modules/<ModuleName>/<ModuleName $class Name>.php',
			'tab' => false,
		),
	),*/
	/* Beans End*/
	/* Layoutdefs */
	/*'layoutdefs' => array (
		array(
			'from' => '<basepath>/install_dir/custom/Extension/modules/<ModuleDirectory>/Ext/Layoutdefs/<String>Layoutdefs.php',
			'to_module' => '<ModuleDirectory>',
		),
	),*/
	/* Layoutdefs End*/
	/* Relationships */
	/*'relationships' => array (
		array(
			// 'module' => '<ModuleDirectory>',
			'meta_data' => '<basepath>/install_dir/custom/metadata/<String>MetaData.php',
		),
	),*/
	/* Relationships End */
	/* Vardefs */
	/*'vardefs' => array (
		array(
			'from' => '<basepath>/install_dir/custom/Extension/modules/<ModuleDirectory>/Ext/Vardefs/<String>Vardefs.php',
			'to_module' => '<ModuleDirectory>',
		),
	),*/
	// 'image_dir' => '<basepath>/icons',
	'copy' => array(
		/* Module & Theme files */
		array(
			'from' => '<basepath>/install_dir/themes/Corporate_Style',
			'to'   => 'themes/Corporate_Style',
		),
		/* Module & Theme files End */
		/* Core JS */
		array(
			'from' => '<basepath>/install_dir/jssource/src_files/themes/Corporate_Style',
			'to'   => 'jssource/src_files/themes/Corporate_Style',
		),
		/* Core JS End */
	),
	/* Language */
	'language' => array(
		/* ENGLISH en_us */
		array(
			'from' => '<basepath>/install_dir/custom/Extension/application/Ext/Language/en_us.CorporateStyle.php',
			'to_module' => 'application',
			'language' => 'en_us',
		),
		/* ENGLISH en_us End */
		/* CHINESE zh_CN */
		array(
			'from' => '<basepath>/install_dir/custom/Extension/application/Ext/Language/zh_CN.CorporateStyle.php',
			'to_module' => 'application',
			'language' => 'zh_CN',
		),
		/* CHINESE zh_CN End */
		/* CHINESE zh_TW */
		array(
			'from' => '<basepath>/install_dir/custom/Extension/application/Ext/Language/zh_TW.CorporateStyle.php',
			'to_module' => 'application',
			'language' => 'zh_TW',
		),
		/* CHINESE zh_CN End */
	),
	/* Language End */
	/* adminPanel */
	/*'administration' => array(
		array(
			'from'=>'<basepath>/install_dir/modules/Administration/AdminOption.<ModuleName>.php',
			'to' => 'modules/Administration/AdminOption.<ModuleName>.php',
		),
	),*/
	/* Menu */
	'menu' => array(
		array(
			'from' => '<basepath>/install_dir/custom/Extension/modules/Home/Ext/Menus/Menu.php',
			'to_module'   => 'Home',
		),
	),
	/* Menu End */
	/*'post_uninstall'=>array(
		0 => '<basepath>/scripts/post_uninstall.php',
	),*/
);

?>
