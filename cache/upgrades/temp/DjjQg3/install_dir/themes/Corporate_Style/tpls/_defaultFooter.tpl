{*
/*********************************************************************************
 * Corporate Style theme for SugarCRM
 * 
 * Community: http://www.ithinkopen.com
 * SugarCRM Chinese Forum: http://www.ithinkopen.com/sugarcrm
 * SugarForge: http://www.sugarforge.org/projects/corporatestyle
 * 
 * Published on COSL SugarCRM OpenSource Library http://cosl.ithinkopen.com/sugarcrm
 * "Powered by think7Strategy.".
 ********************************************************************************/

*}
<div id="footer">

    <div id="bottomWidgets">
        {if $AUTHENTICATED}
        <div id="bottomLinks">
            {$BOTTOMLINKS}
        </div>
        {/if}
        <div id="responseTime">
            {$STATISTICS}
        </div>
    </div>
    <p>
        Theme Design &copy; <a href="http://cosl.ithinkopen.com/sugarcrm" rel="nofollow" target="_blank" title="SugarCRM Theme">COSL SugarCRM Theme</a>
    </p>
    
    <a href="index.php?module=Home&action=index" border="0">
        {sugar_getimage name="SugarLogo" ext=".png" other_attributes='border="0" class="logo" id="logo"'}
    </a>
    <div id="copyright">
        <ul class="footerMenu">
            <li class="item-152"><a href="http://www.think7Strategy.com" rel="nofollow" target="_blank">Official Website</a></li>
            <li class="item-152">
                <a id="powered_by" href="http://www.sugarcrm.com/" rel="nofollow" target="_blank">Powered by SugarCRM</a>
                <div class="infoExpand active">
                    <div class="copyrightInfo">{$COPYRIGHT}</div>
                </div>
            </li>
            {if $AUTHENTICATED}
            <li class="item-152"><a href="index.php?module=Calendar&amp;action=index">Calendar</a></li>
            <li class="item-152"><a href="index.php?module=Emails&amp;action=index">Email</a></li>
            <li class="item-152"><a href="index.php?module=Meetings&amp;action=index">Meeting</a></li>
            <li class="item-152"><a href="index.php?module=Calls&amp;action=index">Call</a></li>
            {/if}
        </ul>
    </div>

</div>
