{*
/*********************************************************************************
 * Corporate Style theme for SugarCRM
 * 
 * Community: http://www.ithinkopen.com
 * SugarCRM Chinese Forum: http://www.ithinkopen.com/sugarcrm
 * SugarForge: http://www.sugarforge.org/projects/corporatestyle
 * 
 * Published on COSL SugarCRM OpenSource Library http://cosl.ithinkopen.com/sugarcrm
 * "Powered by think7Strategy.".
 ********************************************************************************/

*}

<div id="globalLinks">
<ul id="globalLinksExtraMenu">
    <li id="welcome">
        <a id="welcome_link" href="index.php?module=Users&action=EditView&record={$CURRENT_USER_ID}">{$CURRENT_USER}</a>
    <ul class="cssmenu">
        {php}
            global $sugar_flavor;
            $this->assign('sugarFlavorNew', $sugar_flavor);
        {/php}
        {if $sugarFlavorNew == "CE"}
            <li><a id="Profile_link" href="index.php?module=Users&action=EditView&record={$CURRENT_USER_ID}">{$APP.LBL_PROFILE}</a> </li>
        {/if}
        {foreach from=$GCLS item=GCL name=gcl key=gcl_key}
            {foreach from=$GCL.SUBMENU item=GCL_SUBMENU name=gcl_submenu key=gcl_submenu_key}
                <a id="{$gcl_submenu_key}_link" href="{$GCL_SUBMENU.URL}"{if !empty($GCL_SUBMENU.ONCLICK)} onclick="{$GCL_SUBMENU.ONCLICK}"{/if}>{$GCL_SUBMENU.LABEL}</a>
            {/foreach}
            <li>
                <a id="{$gcl_key}_link" href="{$GCL.URL}"{if !empty($GCL.ONCLICK)} onclick="{$GCL.ONCLICK}"{/if}>{$GCL.LABEL}</a>
                {foreach from=$GCL.SUBMENU item=GCL_SUBMENU name=gcl_submenu key=gcl_submenu_key}
                {if $smarty.foreach.gcl_submenu.first}
                {sugar_getimage name="menuarrow" ext=".gif" alt="" other_attributes=''}<br />
                <ul class="cssmenu">
                {/if}
                <li><a id="{$gcl_submenu_key}_link" href="{$GCL_SUBMENU.URL}"{if !empty($GCL_SUBMENU.ONCLICK)} onclick="{$GCL_SUBMENU.ONCLICK}"{/if}>{$GCL_SUBMENU.LABEL}</a></li>
                {if $smarty.foreach.gcl_submenu.last}
                </ul>
                {/if}
                {/foreach}
            </li>
        {/foreach}
        {if !empty($LOGOUT_LINK) && !empty($LOGOUT_LABEL)}
            <li><a id="logout_link" href='{$LOGOUT_LINK}' class='utilsLink'>{$LOGOUT_LABEL}</a> </li>
        {/if}
    </ul>
    </li>
</ul>
</div>
