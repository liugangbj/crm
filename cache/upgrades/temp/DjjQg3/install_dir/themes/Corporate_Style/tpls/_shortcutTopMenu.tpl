{*
/*********************************************************************************
 * Corporate Style theme for SugarCRM
 * 
 * Community: http://www.ithinkopen.com
 * SugarCRM Chinese Forum: http://www.ithinkopen.com/sugarcrm
 * SugarForge: http://www.sugarforge.org/projects/corporatestyle
 * 
 * Published on COSL SugarCRM OpenSource Library http://cosl.ithinkopen.com/sugarcrm
 * "Powered by think7Strategy.".
 ********************************************************************************/

*}
{if $shortcutTopMenu.$name }
<ul class="shortcutTopMenu">
    <li>
        <div class="shortcutColumn">
            <ul class="shortcutMenu">
                {*<li class="groupLabel">{$APP.LBL_LINK_ACTIONS}</li>*}
                {foreach from=$shortcutTopMenu.$name item=shortcut_item}
                    {if $shortcut_item.URL == "-"}
                        <hr style="margin-top: 2px; margin-bottom: 2px" />
                    {else}
                        <li style="white-space:nowrap;">
                            <a id="{$shortcut_item.LABEL|replace:' ':''}{$tabGroupName}" href="{sugar_ajax_url url=$shortcut_item.URL}">{$shortcut_item.LABEL}</a>
                        </li>
                    {/if}
                {/foreach}
            </ul>
        </div>
    </li>
</ul>
{/if}
