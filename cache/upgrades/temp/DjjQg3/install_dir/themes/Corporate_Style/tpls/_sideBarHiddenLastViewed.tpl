{*
/*********************************************************************************
 * Corporate Style theme for SugarCRM
 * 
 * Community: http://www.ithinkopen.com
 * SugarCRM Chinese Forum: http://www.ithinkopen.com/sugarcrm
 * SugarForge: http://www.sugarforge.org/projects/corporatestyle
 * 
 * Published on COSL SugarCRM OpenSource Library http://cosl.ithinkopen.com/sugarcrm
 * "Powered by think7Strategy.".
 ********************************************************************************/

*}
<div id="HideMenu" class="leftList">
<div id="hiddenLastView" class="leftList" onmouseover="hiliteItem(this,'no');">
    <h3><span>{$APP.LBL_LAST_VIEWED}</span></h3>
    <ul id="ul_lastview">
    {foreach from=$recentRecords item=item name=lastViewed}
    <li style="white-space:nowrap;">
        <a title="{$item.item_summary}"
            accessKey="{$smarty.foreach.lastViewed.iteration}"
            href="{sugar_link module=$item.module_name action='DetailView' record=$item.item_id link_only=1}">
            <span class="y">{$item.image}</span><span>{$item.item_summary_short}</span>
        </a>
    </li>
    {foreachelse}
    {$APP.NTC_NO_ITEMS_DISPLAY}
    {/foreach}
    </ul>
</div>
</div>
