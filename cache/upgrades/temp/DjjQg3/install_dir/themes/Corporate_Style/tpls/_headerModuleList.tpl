{*
/*********************************************************************************
 * Corporate Style theme for SugarCRM
 * 
 * Community: http://www.ithinkopen.com
 * SugarCRM Chinese Forum: http://www.ithinkopen.com/sugarcrm
 * SugarForge: http://www.sugarforge.org/projects/corporatestyle
 * 
 * Published on COSL SugarCRM OpenSource Library http://cosl.ithinkopen.com/sugarcrm
 * "Powered by think7Strategy.".
 ********************************************************************************/

*}

<div id="sugarNBMenuTop" class="{if $USE_GROUP_TABS}isGroupedModule{/if}"></div>
<div id="sugarNBMenu" class="sugarNBMenu sugarNBMenuFloat{if !$USE_GROUP_TABS} isGroupedModule{/if}">
{include file="_companyLogo.tpl" theme_template=true}

{if $USE_GROUP_TABS}
<div id="moduleList">
<ul class="left">
    {assign var="groupSelected" value=false}
    {foreach from=$groupTabs item=modules key=group name=groupList}
    {capture name=extraparams assign=extraparams}parentTab={$group}{/capture}
    {if ( ( $smarty.request.parentTab == $group || (!$smarty.request.parentTab && in_array($MODULE_TAB,$modules.modules)) ) && !$groupSelected ) || ($smarty.foreach.groupList.index == 0 && $defaultFirst)}
    <li>
        <span class="currentTab">
            <a href="#" id="grouptab_{$smarty.foreach.groupList.index}">{$group}</a>
        </span>
        {assign var="groupSelected" value=true}
    {else}
    <li>
        <span class="notCurrentTab">
        <a href="#" id="grouptab_{$smarty.foreach.groupList.index}">{$group}</a>
        </span>
    {/if}
    </li>
    {/foreach}
</ul>
</div>
<div class="clear"></div>
<div id="subModuleList" class="subModuleListFloat">
    {assign var="groupSelected" value=false}
    {foreach from=$groupTabs item=modules key=group name=moduleList}
    {capture name=extraparams assign=extraparams}parentTab={$group}{/capture}
    <span id="moduleLink_{$smarty.foreach.moduleList.index}" {if ( ( $smarty.request.parentTab == $group || (!$smarty.request.parentTab && in_array($MODULE_TAB,$modules.modules)) ) && !$groupSelected ) || ($smarty.foreach.moduleList.index == 0 && $defaultFirst)}class="selected" {assign var="groupSelected" value=true}{/if}>
        <ul>
            {foreach from=$modules.modules item=module key=modulekey}
            <li class="subTab">
                {capture name=moduleTabId assign=moduleTabId}moduleTab_{$smarty.foreach.moduleList.index}_{$module}{/capture}
                {sugar_link id=$moduleTabId module=$modulekey data=$module extraparams=$extraparams}
            </li>
            {/foreach}
            {if !empty($modules.extra)}
            <li class="subTabMore">
                <a href="#">{$APP.LBL_MORE}</a>
                <ul class="cssmenu">
                    {foreach from=$modules.extra item=submodulename key=submodule}
                    <li>
                        <a href="{sugar_link module=$submodule link_only=1 extraparams=$extraparams}">{$submodulename}
                        </a>
                    </li>
                {/foreach}
                </ul> 
            </li>
            {/if}
        </ul>
    </span>
    {/foreach}
</div>
{else}
<div id="moduleList">
<ul>
    {foreach from=$moduleTopMenu item=module key=name name=moduleList}

    {if $name == "Home"}
        {assign var='homeClass' value='home'}
        {assign var='title' value=$APP.LBL_SUGAR_HOME}
    {else}
        {assign var='homeClass' value=''}
        {assign var='title' value=''}
    {/if}

    {if $name == $MODULE_TAB}
    <li class="noBorder currentLiTab {$homeClass}">
        <span class="currentTab">{sugar_link id="moduleTab_$name" module=$name data=$module title=$title}</span>
    {else}
    <li class="notCurrentLiTab {$homeClass}">
        <span class="notCurrentTab">{sugar_link id="moduleTab_$name" module=$name data=$module title=$title}</span>
    {/if}
    {include file="_shortcutTopMenu.tpl" theme_template=true}
    </li>
    {/foreach}
    {if count($moduleExtraMenu) > 0}
    <li id="moduleTabExtraMenu">
        <a href="#">{$APP.LBL_MORE}</a>
        <ul class="cssmenu">
            {foreach from=$moduleExtraMenu item=module key=name name=moduleList}
            <li>
                {sugar_link id="moduleTab_$name" module=$name data=$module}
            </li>
            {/foreach}
        </ul>
    </li>
    {/if}
</ul>
</div>
{/if}
</div>
