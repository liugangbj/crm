{*
/*********************************************************************************
 * Corporate Style theme for SugarCRM
 * 
 * Community: http://www.ithinkopen.com
 * SugarCRM Chinese Forum: http://www.ithinkopen.com/sugarcrm
 * SugarForge: http://www.sugarforge.org/projects/corporatestyle
 * 
 * Published on COSL SugarCRM OpenSource Library http://cosl.ithinkopen.com/sugarcrm
 * "Powered by think7Strategy.".
 ********************************************************************************/

*}
{include file="_head.tpl" theme_template=true}
<body class="yui-skin-sam" onMouseOut="closeMenus();">
<a name="top"></a>
<div style="position: fixed; top: 0; left: 0; width: 1px; height: 1px; z-index: 21;"></div>
{if $AUTHENTICATED}
{$DCSCRIPT}
<div id="header">
    <div id="ajaxHeader">
        {include file="_headerModuleList.tpl" theme_template=true}
        <div id="globalWidgets" class="globalWidgetsFloat">
            {include file="_headerSearch.tpl" theme_template=true}
            {include file="_globalLinks.tpl" theme_template=true}
            {include file="_expandLinks.tpl" theme_template=true}
        </div>
    </div>
</div>
{/if}
<div class="clear"></div>

{literal}
<iframe id='ajaxUI-history-iframe' src='index.php?entryPoint=getImage&imageName=blank.png'  title='empty' style='display:none'></iframe>
<input id='ajaxUI-history-field' type='hidden'>
<script type='text/javascript'>
if (SUGAR.ajaxUI && !SUGAR.ajaxUI.hist_loaded)
{
    YAHOO.util.History.register('ajaxUILoc', "", SUGAR.ajaxUI.go);
    {/literal}{if $smarty.request.module != "ModuleBuilder"}{* Module builder will init YUI history on its own *}
    YAHOO.util.History.initialize("ajaxUI-history-field", "ajaxUI-history-iframe");
    {/if}{literal}
}
</script>
{/literal}

<div id="main">
    {if $AUTHENTICATED}
    {include file="_sideBarHide.tpl" theme_template=true}
    {include file="_sideBar.tpl" theme_template=true}
    {include file="_sideBarHiddenLastViewed.tpl" theme_template=true}
    {/if}
    <div id="content" {if !$AUTHENTICATED}class="noSideBar"{/if}>
        <table style="width:100%" class="{if $USE_GROUP_TABS}isGroupedModule{/if}"><tr><td>