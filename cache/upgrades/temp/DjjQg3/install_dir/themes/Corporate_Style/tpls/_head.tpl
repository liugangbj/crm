{*
/*********************************************************************************
 * Corporate Style theme for SugarCRM
 * 
 * Community: http://www.ithinkopen.com
 * SugarCRM Chinese Forum: http://www.ithinkopen.com/sugarcrm
 * SugarForge: http://www.sugarforge.org/projects/corporatestyle
 * 
 * Published on COSL SugarCRM OpenSource Library http://cosl.ithinkopen.com/sugarcrm
 * "Powered by think7Strategy.".
 ********************************************************************************/

*}
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html {$langHeader}>
<head>
<link rel="SHORTCUT ICON" href="{$FAVICON_URL}">
<meta http-equiv="Content-Type" content="text/html; charset={$APP.LBL_CHARSET}">
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<title>{$APP.LBL_BROWSER_TITLE}</title>
{$SUGAR_CSS}

{if $smarty.request.module == "ModuleBuilder"}
{*{literal}<style>div#content > table tr td p:first-child[class="error"] { display: none !important; visibility: hidden !important; }</style>{/literal}*}
<link rel="stylesheet" type="text/css" href="{sugar_getjspath file="themes/Corporate_Style/css/extendMB_style.css"}" />
{/if}
{php}
    //global $sugar_config;
    //$this->assign('sugarConfigNew', $sugar_config);
    //global $sugar_version;
    //$this->assign('sugarVersionNew', $sugar_version);
    //global $current_user;
    //$this->assign('currentUserNew', $current_user);
    //global $sugar_flavor;
    //$this->assign('sugarFlavorNew', $sugar_flavor);
    global $current_language;
    $this->assign('currentLanguageNew', $current_language);
{/php}
{*{if $sugarFlavorNew == "CE"}
    <link rel="stylesheet" type="text/css" href="{sugar_getjspath file="themes/Corporate_Style/css/extend_style_CEonly.css"}" />
{/if}*}
{if $currentLanguageNew == "zh_CN"}
    <link rel="stylesheet" type="text/css" href="{sugar_getjspath file="themes/Corporate_Style/css/extendLangCN_style.css"}" />
{/if}

{$SUGAR_JS}
{literal}
<script type="text/javascript">
<!--
SUGAR.themes.theme_name = '{/literal}{$THEME}{literal}';
SUGAR.themes.theme_ie6compat = {/literal}{$THEME_IE6COMPAT}{literal};
SUGAR.themes.hide_image = '{/literal}{sugar_getimagepath file="hide.gif"}{literal}';
SUGAR.themes.show_image = '{/literal}{sugar_getimagepath file="show.gif"}{literal}';
SUGAR.themes.loading_image = '{/literal}{sugar_getimagepath file="img_loading.gif"}{literal}';
SUGAR.themes.allThemes = eval({/literal}{$allThemes}{literal});
if ( YAHOO.env.ua )
    UA = YAHOO.env.ua;
-->
</script>
{/literal}
<script type="text/javascript" src='{sugar_getjspath file="cache/include/javascript/sugar_field_grp.js"}'></script>
</head>