/*********************************************************************************
 * Corporate Style theme for SugarCRM
 * 
 * Community: http://www.ithinkopen.com
 * SugarCRM Chinese Forum: http://www.ithinkopen.com/sugarcrm
 * SugarForge: http://www.sugarforge.org/projects/corporatestyle
 * 
 * Published on COSL SugarCRM OpenSource Library http://cosl.ithinkopen.com/sugarcrm
 * "Powered by think7Strategy.".
 ********************************************************************************/
$(document).ready(function(){$("ul.clickMenu").each(function(index,node){$(node).sugarActionMenu();});});YAHOO.util.Event.onAvailable('sitemapLinkSpan',function()
{document.getElementById('sitemapLinkSpan').onclick=function()
{ajaxStatus.showStatus(SUGAR.language.get('app_strings','LBL_LOADING_PAGE'));var smMarkup='';var callback={success:function(r){ajaxStatus.hideStatus();document.getElementById('sm_holder').innerHTML=r.responseText;with(document.getElementById('sitemap').style){display="block";position="absolute";right=0;top=34;}
document.getElementById('sitemapClose').onclick=function()
{document.getElementById('sitemap').style.display="none";}}}
postData='module=Home&action=sitemap&GetSiteMap=now&sugar_body_only=true';YAHOO.util.Connect.asyncRequest('POST','index.php',callback,postData);}});function IKEADEBUG()
{var moduleLinks=document.getElementById('moduleList').getElementsByTagName("a");moduleLinkMouseOver=function()
{var matches=/grouptab_([0-9]+)/i.exec(this.id);var tabNum=matches[1];var moduleGroups=document.getElementById('subModuleList').getElementsByTagName("span");for(var i=0;i<moduleGroups.length;i++){if(i==tabNum){moduleGroups[i].className='selected';}
else{moduleGroups[i].className='';}}
var groupList=document.getElementById('moduleList').getElementsByTagName("li");var currentGroupItem=tabNum;for(var i=0;i<groupList.length;i++){var aElem=groupList[i].getElementsByTagName("a")[0];if(aElem==null){continue;}
var classStarter='notC';if(aElem.id=="grouptab_"+tabNum){classStarter='c';currentGroupItem=i;}
var spanTags=groupList[i].getElementsByTagName("span");for(var ii=0;ii<spanTags.length;ii++){if(spanTags[ii].className==null){continue;}
var oldClass=spanTags[ii].className.match(/urrentTab.*/);spanTags[ii].className=classStarter+oldClass;}}};for(var i=0;i<moduleLinks.length;i++){moduleLinks[i].onmouseover=moduleLinkMouseOver;}};SUGAR.themes=SUGAR.namespace("themes");SUGAR.append(SUGAR.themes,{allMenuBars:{},setModuleTabs:function(html){var el=document.getElementById('ajaxHeader');if(el){try{YAHOO.util.Event.purgeElement(el,true);for(var i in this.allMenuBars){if(this.allMenuBars[i].destroy)
this.allMenuBars[i].destroy();}}catch(e){window.location.reload();}
if(el.hasChildNodes()){while(el.childNodes.length>=1){el.removeChild(el.firstChild);}}
el.innerHTML+=html;this.loadModuleList();}},actionMenu:function(){$("ul.clickMenu").each(function(index,node){$(node).sugarActionMenu();});},loadModuleList:function(){var nodes=YAHOO.util.Selector.query('#moduleList>div'),currMenuBar;this.allMenuBars={};for(var i=0;i<nodes.length;i++){currMenuBar=SUGAR.themes.currMenuBar=new YAHOO.widget.MenuBar(nodes[i].id,{autosubmenudisplay:true,visible:false,hidedelay:750,lazyload:true});currMenuBar.render();this.allMenuBars[nodes[i].id.substr(nodes[i].id.indexOf('_')+1)]=currMenuBar;if(typeof YAHOO.util.Dom.getChildren(nodes[i])=='object'&&YAHOO.util.Dom.getChildren(nodes[i]).shift().style.display!='none'){oMenuBar=currMenuBar;}}
YAHOO.util.Event.onAvailable('subModuleList',IKEADEBUG);},setCurrentTab:function(){}});YAHOO.util.Event.onContentReady('HideHandle',function()
{document.getElementById('HideHandle').onclick=function()
{document.getElementById('HideMenu').style.visibility='hidden';if(document.getElementById("sideBar").style.display=='none'){document.getElementById("sideBar").style.display='inline';document.getElementById("content").className='';Set_Cookie('showLeftCol','true',30,'/','','');document['HideHandle'].src=SUGAR.themes.hide_image;}
else{document.getElementById("sideBar").style.display='none';document.getElementById("content").className='noSideBar';Set_Cookie('showLeftCol','false',30,'/','','');document['HideHandle'].src=SUGAR.themes.show_image;}}
document.getElementById('HideHandle').onmouseover=function()
{if(document.getElementById("sideBar").style.display=='none'){tbButtonMouseOver('HideHandle',34,'',12);}}});YAHOO.util.Event.onContentReady('content',function()
{if(!Get_Cookie('showLeftCol')){Set_Cookie('showLeftCol','true',30,'/','','');}
else{if(Get_Cookie('showLeftCol')=='false'&&document.getElementById('HideHandle')!=null){document.getElementById('HideHandle').onclick();}}});SUGAR.themes.tempHideLeftCol=function()
{document.getElementById('HideMenu').style.visibility='hidden';document.getElementById("sideBar").style.display='none';document.getElementById("content").className='noSideBar';document['HideHandle'].src=SUGAR.themes.show_image;}
YAHOO.util.Event.onDOMReady(SUGAR.themes.loadModuleList,SUGAR.themes,true);