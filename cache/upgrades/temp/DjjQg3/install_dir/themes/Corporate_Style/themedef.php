<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * Corporate Style theme for SugarCRM
 * 
 * Community: http://www.ithinkopen.com
 * SugarCRM Chinese Forum: http://www.ithinkopen.com/sugarcrm
 * SugarForge: http://www.sugarforge.org/projects/corporatestyle
 * 
 * Published on COSL SugarCRM OpenSource Library http://cosl.ithinkopen.com/sugarcrm
 * "Powered by think7Strategy.".
 ********************************************************************************/


$themedef = array(
    'name'  => "Corporate",
    'description' => "Corporate Style theme For SugarCRM, Created by think7Strategy.",
    'version' => array(
        'regex_matches' => array('6\.*.*'),
        ),
    'group_tabs' => true,
    );
