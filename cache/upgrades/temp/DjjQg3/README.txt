
Corporate Style theme for SugarCRM

Theme info
--------------------------------------------------------
Created by think7Strategy. 
Support: www.ithinkopen.com


Install Instructions
--------------------------------------------------------
1. Install theme through the Module Loader
2. Set Corporate theme in user's settings: 
    Log in and click on logged user name at the top,
    next edit the user's profile and set the appropriate theme.
    Alternatively you can set Corporate Theme as default 
    Step: Admin -> Themes settings.
3. do Quick Repair and Rebuild.


