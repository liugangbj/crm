<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * Corporate Style theme for SugarCRM
 * 
 * Community: http://www.ithinkopen.com
 * SugarCRM Chinese Forum: http://www.ithinkopen.com/sugarcrm
 * SugarForge: http://www.sugarforge.org/projects/corporatestyle
 * 
 * Published on COSL SugarCRM OpenSource Library http://cosl.ithinkopen.com/sugarcrm
 * "Powered by think7Strategy Org.".
 ********************************************************************************/
 
 
require_once('include/dir_inc.php');
function post_install() {
	
	global $sugar_config;
	
	$module_list = array(
		'Home',
		'Accounts',
		'Bugs',
		'Calls',
		'Cases',
		'Contacts',
		'Leads',
		'Meetings',
		'Notes',
		'Opportunities',
		'ProspectLists',
		'Prospects',
		'Tasks',
	);
	
	// Add banning of AjaxUI for ProductSuite modules 
	require_once('modules/Configurator/Configurator.php');
	$cfg = new Configurator();
	$overrideArray = $cfg->readOverride();
	if (array_key_exists('addAjaxBannedModules', $overrideArray)) {
		$disabled_modules = $overrideArray['addAjaxBannedModules'];
		$updatedArray = array_merge($disabled_modules, array_diff($module_list, $disabled_modules));
	} else { 
		$updatedArray = $module_list;
	}
	$cfg->config['addAjaxBannedModules'] = empty($updatedArray) ? FALSE : $updatedArray;
	$cfg->handleOverride();
	
}

?>
