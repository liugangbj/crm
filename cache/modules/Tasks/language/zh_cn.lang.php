<?php
// created: 2015-06-15 02:55:54
$mod_strings = array (
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '创建日期',
  'LBL_DATE_MODIFIED' => '修改日期',
  'LBL_MODIFIED' => '修改人',
  'LBL_MODIFIED_ID' => '修改人ID',
  'LBL_MODIFIED_NAME' => '修改人姓名',
  'LBL_CREATED' => '创建人',
  'LBL_CREATED_ID' => '创建人ID',
  'LBL_DESCRIPTION' => '说明:',
  'LBL_DELETED' => '已删除',
  'LBL_NAME' => '名称:',
  'LBL_CREATED_USER' => '创建用户',
  'LBL_MODIFIED_USER' => '修改用户',
  'LBL_LIST_NAME' => '姓名',
  'LBL_ASSIGNED_TO_ID' => '负责人ID',
  'LBL_ASSIGNED_TO_NAME' => '指派给:',
  'LBL_MODULE_NAME' => '任务',
  'LBL_TASK' => '任务:',
  'LBL_MODULE_TITLE' => '任务: 首页',
  'LBL_SEARCH_FORM_TITLE' => '任务搜索',
  'LBL_LIST_FORM_TITLE' => '任务列表',
  'LBL_NEW_FORM_TITLE' => '新增任务',
  'LBL_NEW_FORM_SUBJECT' => '主题:',
  'LBL_NEW_FORM_DUE_DATE' => '截止日期:',
  'LBL_NEW_FORM_DUE_TIME' => '截止时间:',
  'LBL_NEW_TIME_FORMAT' => '(24:00)',
  'LBL_LIST_CLOSE' => '关闭',
  'LBL_LIST_SUBJECT' => '主题',
  'LBL_LIST_CONTACT' => '联系人',
  'LBL_LIST_PRIORITY' => '优先级',
  'LBL_LIST_RELATED_TO' => '关联到',
  'LBL_LIST_DUE_DATE' => '截止日期',
  'LBL_LIST_DUE_TIME' => '截止时间',
  'LBL_SUBJECT' => '主题:',
  'LBL_STATUS' => '状态:',
  'LBL_DUE_DATE' => '截止日期:',
  'LBL_DUE_TIME' => '截止时间:',
  'LBL_PRIORITY' => '优先级:',
  'LBL_COLON' => ':',
  'LBL_DUE_DATE_AND_TIME' => '截止日期 & 时间:',
  'LBL_START_DATE_AND_TIME' => '开始日期& 时间:',
  'LBL_START_DATE' => '开始日期:',
  'LBL_LIST_START_DATE' => '开始日期',
  'LBL_START_TIME' => '开始时间:',
  'LBL_LIST_START_TIME' => '开始时间',
  'DATE_FORMAT' => '(年-月-日)',
  'LBL_NONE' => '无',
  'LBL_CONTACT' => '联系人:',
  'LBL_EMAIL_ADDRESS' => '电子邮件地址:',
  'LBL_PHONE' => '电话:',
  'LBL_EMAIL' => '电子邮件地址:',
  'LBL_DESCRIPTION_INFORMATION' => '说明信息',
  'LBL_CONTACT_NAME' => '联系人名称',
  'LBL_LIST_COMPLETE' => '完成:',
  'LBL_LIST_STATUS' => '状态',
  'LBL_DATE_DUE_FLAG' => '无截止日期',
  'LBL_DATE_START_FLAG' => '无开始日期',
  'ERR_DELETE_RECORD' => '必须指定记录编号才能删除客户。',
  'ERR_INVALID_HOUR' => '请输入0到24之间的小时数',
  'LBL_DEFAULT_PRIORITY' => '中',
  'LBL_LIST_MY_TASKS' => '我的公开任务',
  'LNK_NEW_TASK' => '新增任务',
  'LNK_TASK_LIST' => '查看任务',
  'LNK_IMPORT_TASKS' => '导入任务',
  'LBL_CONTACT_FIRST_NAME' => '联系人的名',
  'LBL_CONTACT_LAST_NAME' => '联系人的姓',
  'LBL_LIST_ASSIGNED_TO_NAME' => '指派的用户',
  'LBL_LIST_DATE_MODIFIED' => '修改日期',
  'LBL_CONTACT_ID' => '联系人编号:',
  'LBL_PARENT_ID' => '上级编号:',
  'LBL_CONTACT_PHONE' => '联系人电话:',
  'LBL_PARENT_NAME' => '上级类型:',
  'LBL_ACTIVITIES_REPORTS' => '活动报告',
  'LBL_TASK_INFORMATION' => '任务查看',
  'LBL_EDITLAYOUT' => '编辑布局',
  'LBL_HISTORY_SUBPANEL_TITLE' => '记录',
  'LBL_DATE_DUE' => '截止日期',
  'LBL_EXPORT_ASSIGNED_USER_NAME' => '被分配者名称',
  'LBL_EXPORT_ASSIGNED_USER_ID' => '被分配用户ID',
  'LBL_EXPORT_MODIFIED_USER_ID' => '由ID修改',
  'LBL_EXPORT_CREATED_BY' => '由ID创建',
  'LBL_EXPORT_PARENT_TYPE' => '关联到模块',
  'LBL_EXPORT_PARENT_ID' => '关联到ID',
  'LBL_RELATED_TO' => '关联到：',
);