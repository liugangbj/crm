<?php

$manifest = array(
    // only install on the following sugar versions (if empty, no check)
    array (
      'exact_matches' =>
        array (
        ),
      'regex_matches' =>
        array (
          0 => '6\.*\.*'
        ),
    ),
    // Version for which this langpack can work
    'acceptable_sugar_flavors' =>
      array (
        0 => 'CE',
      ),

    // name of the Pack
    'name' => 'SugarCRM简体中文语言包（Simplified Chinese Language Pack）',

    // description of new code
    'description' => 'SugarCRM简体中文语言包（Simplified Chinese Language Pack）6.5.17',

    // author of new code
    'author' => '木子飞飞 (rell336@126.com),无常(Huang.jecky@gmail.com),Cheli Zhao (cheli.zhao@srforce.com), Richard Qi (richard@srforce.com), production led by SR Force Consultants (China)(www.srforce.cn)',

    // date published
    'published_date' => '2014/07/23',

    // version of code
    'version' => '6.5.17',

    // type of code (valid choices are: full, langpack, module, patch, theme )
    'type' => 'langpack',

    // icon for displaying in UI (path to graphic contained within zip package)
    'icon' => '',

    // Uninstall is allowed
    'is_uninstallable' => TRUE,
);

$installdefs = array(
	'id' => 'zh_cn',
	'copy' => array(
				array(
					'from' => '<basepath>/modules',
					'to'   => 'modules',
					),
				array(
					'from' => '<basepath>/include/language',
					'to'   => 'include/language'
					),
				array(
					'from' => '<basepath>/install/language',
					'to'   => 'install/language',
					),
				array(
					'from' => '<basepath>/themes/Sugar5/tpls',
					'to'   => 'themes/Sugar5/tpls',
					),
                ),    

	'pre_execute'=>array(
		0 => '<basepath>/custom/actions_cn/pre_install.php',
	  ),
	  
	'post_execute'=>array(
		0 => '<basepath>/custom/actions_cn/post_install.php',
	   ),
		
	'post_uninstall'=>array(
		0 => '<basepath>/custom/actions_cn/post_uninstall.php',
	  ),

);
?>
 
