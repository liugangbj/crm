<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2012 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

$mod_strings = array (
	//module
	'LBL_MODULE_NAME' => '文档',
	'LBL_MODULE_TITLE' => '文档: 主页',
	'LNK_NEW_DOCUMENT' => '创建文档',
	'LNK_DOCUMENT_LIST'=> '文档列表',
	'LBL_SEARCH_FORM_TITLE'=> '文档搜索',
	//vardef labels
	'LBL_DOCUMENT_ID' => '文档ID',
	'LBL_NAME' => '文档名称',
	'LBL_DESCRIPTION' => '描述',
	'LBL_ASSIGNED_TO' => '负责人：',
	'LBL_CATEGORY' => '分类',
	'LBL_SUBCATEGORY' => '子分类',
	'LBL_STATUS' => '状态',
	'LBL_IS_TEMPLATE'=>'是一个模板',
	'LBL_TEMPLATE_TYPE'=>'文档类型',
	'LBL_REVISION_NAME' => '修订号',
	'LBL_MIME' => 'Mime类型',
	'LBL_REVISION' => '修订',
	'LBL_DOCUMENT' => '相关文档',
	'LBL_LATEST_REVISION' => '最新修订',
	'LBL_CHANGE_LOG'=> '修改记录',
	'LBL_ACTIVE_DATE'=> '发布日期',
	'LBL_EXPIRATION_DATE' => '过期日期',
	'LBL_FILE_EXTENSION'  => '文件扩展',

	'LBL_CAT_OR_SUBCAT_UNSPEC'=>'未指定',
	//quick search
	'LBL_NEW_FORM_TITLE' => '新文档',
	//document edit and detail view
	'LBL_DOC_NAME' => '文档名称：',
	'LBL_FILENAME' => '文件名：',
	'LBL_FILE_UPLOAD' => '文件：',
	'LBL_DOC_VERSION' => '修订：',
	'LBL_CATEGORY_VALUE' => '分类：',
	'LBL_SUBCATEGORY_VALUE'=> '子分类：',
	'LBL_DOC_STATUS'=> '状态：',
	'LBL_DET_TEMPLATE_TYPE'=>'文档类型：',
	'LBL_DOC_DESCRIPTION'=>'描述：',
	'LBL_DOC_ACTIVE_DATE'=> '发布如期：',
	'LBL_DOC_EXP_DATE'=> '过期日期：',

	//document list view.
	'LBL_LIST_FORM_TITLE' => '文档列表',
	'LBL_LIST_DOCUMENT' => '文档',
	'LBL_LIST_CATEGORY' => '分类',
	'LBL_LIST_SUBCATEGORY' => '子分类',
	'LBL_LIST_REVISION' => '修订',
	'LBL_LIST_LAST_REV_CREATOR' => '发布人',
	'LBL_LIST_LAST_REV_DATE' => '修订日期',
	'LBL_LIST_VIEW_DOCUMENT'=>'查看',
    'LBL_LIST_DOWNLOAD'=> '下载',
	'LBL_LIST_ACTIVE_DATE' => '发布日期',
	'LBL_LIST_EXP_DATE' => '过期日期',
	'LBL_LIST_STATUS'=>'状态',

	//document search form.
	'LBL_SF_DOCUMENT' => '文档名称：',
	'LBL_SF_CATEGORY' => '分类：',
	'LBL_SF_SUBCATEGORY'=> '子分类:',
	'LBL_SF_ACTIVE_DATE' => '发布日期：',
	'LBL_SF_EXP_DATE'=> '过期日期：',

	'DEF_CREATE_LOG' => '文档已创建',

	//error messages
	'ERR_DOC_NAME'=>'文档名称',
	'ERR_DOC_ACTIVE_DATE'=>'发布日期',
	'ERR_DOC_EXP_DATE'=> '过期日期',
	'ERR_FILENAME'=> '文件名',

	'LBL_TREE_TITLE' => '文档',
	//sub-panel vardefs.
	'LBL_LIST_DOCUMENT_NAME'=>'文档名称',
);


?>
