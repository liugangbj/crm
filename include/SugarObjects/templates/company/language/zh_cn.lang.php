<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2012 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


$mod_strings = array (
	'ACCOUNT_REMOVE_PROJECT_CONFIRM' => '您确定希望将这个账户从项目中移除吗？',
	'ERR_DELETE_RECORD' => '删除记录时必须指定一个记录编号。',
	'LBL_ACCOUNT_NAME' => '公司名称：',
	'LBL_ACCOUNT' => '公司：',
	'LBL_ACTIVITIES_SUBPANEL_TITLE'=>'活动',
	'LBL_ADDRESS_INFORMATION' => '地址信息',
	'LBL_ANNUAL_REVENUE' => '年营业额：',
	'LBL_ANY_ADDRESS' => '任意地址：',
	'LBL_ANY_EMAIL' => '任意邮件：',
	'LBL_ANY_PHONE' => '任意电话：',
	'LBL_ASSIGNED_TO_NAME' => '用户：',
	'LBL_RATING' => '评分',
	'LBL_ASSIGNED_TO' => '负责人:',
	'LBL_ASSIGNED_USER' => '负责人:',
	'LBL_ASSIGNED_TO_ID' => '负责人ID:',
	'LBL_BILLING_ADDRESS_CITY' => '账单城市：',
	'LBL_BILLING_ADDRESS_COUNTRY' => '账单国家：',
	'LBL_BILLING_ADDRESS_POSTALCODE' => '账单邮编：',
	'LBL_BILLING_ADDRESS_STATE' => '账单省：',
	'LBL_BILLING_ADDRESS_STREET_2' =>'账单街道 2',
	'LBL_BILLING_ADDRESS_STREET_3' =>'账单街道 3',
	'LBL_BILLING_ADDRESS_STREET_4' =>'账单街道 4',
	'LBL_BILLING_ADDRESS_STREET' => '账单街道：',
	'LBL_BILLING_ADDRESS' => '账单地址：',
	'LBL_ACCOUNT_INFORMATION' => '公司信息',
	'LBL_CITY' => '城市:',
	'LBL_CONTACTS_SUBPANEL_TITLE' => '联系人',
	'LBL_COUNTRY' => '国家：',
	'LBL_DATE_ENTERED' => '创建日期：',
	'LBL_DATE_MODIFIED' => '修改日期：',
	'LBL_DEFAULT_SUBPANEL_TITLE' => '账户：',
	'LBL_DESCRIPTION_INFORMATION' => '描述信息',
	'LBL_DESCRIPTION' => '描述：',
	'LBL_DUPLICATE' => '可能重复账户',
	'LBL_EMAIL' => '邮件地址：',
	'LBL_EMPLOYEES' => '员工：',
	'LBL_FAX' => '传真：',
	'LBL_INDUSTRY' => '行业：',
	'LBL_LIST_ACCOUNT_NAME' => '账户名称',
	'LBL_LIST_CITY' => '城市',
	'LBL_LIST_EMAIL_ADDRESS' => '邮件地址',
	'LBL_LIST_PHONE' => '电话',
	'LBL_LIST_STATE' => '省',
	'LBL_LIST_WEBSITE' => '网站',
	'LBL_MEMBER_OF' => '归属组织：',
	'LBL_MEMBER_ORG_FORM_TITLE' => '成员组织',
	'LBL_MEMBER_ORG_SUBPANEL_TITLE'=>'成员组织',
	'LBL_NAME'=>'姓名：',
	'LBL_OTHER_EMAIL_ADDRESS' => '其他邮件地址：',
	'LBL_OTHER_PHONE' => '其他电话：',
	'LBL_OWNERSHIP' => '所有权：',
	'LBL_PARENT_ACCOUNT_ID' => '父帐户ID',
	'LBL_PHONE_ALT' => '备用电话：',
	'LBL_PHONE_FAX' => '电话传真：',
	'LBL_PHONE_OFFICE' => '办公室电话：',
	'LBL_PHONE' => '电话：',
    'LBL_EMAIL_ADDRESS' => '邮件地址',
	'LBL_EMAIL_ADDRESSES' => '邮件地址',
	'LBL_POSTAL_CODE' => '邮编：',
	'LBL_PUSH_BILLING' => '确定帐单',
	'LBL_PUSH_SHIPPING' => '确定装运',
	'LBL_SAVE_ACCOUNT' => '保存账户',
	'LBL_SHIPPING_ADDRESS_CITY' => '货运城市：',
	'LBL_SHIPPING_ADDRESS_COUNTRY' => '货运国家：',
	'LBL_SHIPPING_ADDRESS_POSTALCODE' => '货运邮编：',
	'LBL_SHIPPING_ADDRESS_STATE' => '货运省：',
	'LBL_SHIPPING_ADDRESS_STREET_2' => '货运地址 2',
	'LBL_SHIPPING_ADDRESS_STREET_3' => '货运地址 3',
	'LBL_SHIPPING_ADDRESS_STREET_4' => '货运地址 4',
	'LBL_SHIPPING_ADDRESS_STREET' => '货运地址：',
	'LBL_SHIPPING_ADDRESS' => '货运地址：',

	'LBL_STATE' => '省：',
	'LBL_TICKER_SYMBOL' => '触发器符号',
	'LBL_TYPE' => '类型：',
	'LBL_USERS_ASSIGNED_LINK'=>'负责用户：',
	'LBL_USERS_CREATED_LINK'=>'创建用户：',
	'LBL_USERS_MODIFIED_LINK'=>'修改用户：',
	'LBL_VIEW_FORM_TITLE' => '账户视图',
	'LBL_WEBSITE' => '网站：',

	'LNK_ACCOUNT_LIST' => '账户',
	'LNK_NEW_ACCOUNT' => '创建账户',

	'MSG_DUPLICATE' => '您即将建立的账户也许和已经存在的记录重复。下面列出了类似名称的账户。<br>点击建立账户来继续建立这个新的账户，或选择下面列表中已存在的账户。',
	'MSG_SHOW_DUPLICATES' => '您即将建立的账户也许和已经存在的记录重复。下面列出了类似名称的账户<br>点击保存来继续创建这个新的账户，或点击取消来在不保存记录的情况下返回之前的模块。',

	'NTC_COPY_BILLING_ADDRESS' => '复制账单地址至货运地址',
	'NTC_COPY_BILLING_ADDRESS2' => '复制至货运',
	'NTC_COPY_SHIPPING_ADDRESS' => '复制货运地址至账单地址',
	'NTC_COPY_SHIPPING_ADDRESS2' => '复制至账单',
	'NTC_DELETE_CONFIRMATION' => '您确定要删除这条记录吗？',
	'NTC_REMOVE_ACCOUNT_CONFIRMATION' => '你确定要移除这条记录吗？',
	'NTC_REMOVE_MEMBER_ORG_CONFIRMATION' => '您确定您要移除此项作为成员组织的记录吗？',
);


