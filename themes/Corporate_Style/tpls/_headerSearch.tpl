{*
/*********************************************************************************
 * Corporate Style theme for SugarCRM
 * 
 * Community: http://www.ithinkopen.com
 * SugarCRM Chinese Forum: http://www.ithinkopen.com/sugarcrm
 * SugarForge: http://www.sugarforge.org/projects/corporatestyle
 * 
 * Published on COSL SugarCRM OpenSource Library http://cosl.ithinkopen.com/sugarcrm
 * "Powered by think7Strategy.".
 ********************************************************************************/

*}
{if $AUTHENTICATED}
<div id="sitemapLink">
    <span id="sitemapLinkSpan">
        {$APP.LBL_SITEMAP}
        {*{sugar_getimage name="MoreDetail" alt=$app_strings.LBL_MOREDETAIL ext=".png" other_attributes=''}*}
    </span>
</div>
<span id="sm_holder"></span>
<div id="search">
    <form name='UnifiedSearch' action='index.php' onsubmit='return SUGAR.unifiedSearchAdvanced.checkUsaAdvanced()'>
        <input type="hidden" name="action" value="UnifiedSearch">
        <input type="hidden" name="module" value="Home">
        <input type="hidden" name="search_form" value="false">
        <input type="hidden" name="advanced" value="false">
        {*{sugar_getimage name="searchMore" ext=".gif" alt=$APP.LNK_ADVANCED_SEARCH other_attributes='border="0" id="unified_search_advanced_img" '}&nbsp;*}
        <input type="text" name="query_string" id="query_string" size="20"{* value="{$APP.LBL_SEARCH}"*}>
        <input type="submit" class="button" value="{$APP.LBL_SEARCH}">
        {*<span id="unified_search_advanced_img">{$APP.LNK_ADVANCED_SEARCH}</span>*}
    </form>
    <div id="unified_search_advanced_div" class="none"> </div>
</div>
{/if}
