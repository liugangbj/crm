{*
/*********************************************************************************
 * Corporate Style theme for SugarCRM
 * 
 * Community: http://www.ithinkopen.com
 * SugarCRM Chinese Forum: http://www.ithinkopen.com/sugarcrm
 * SugarForge: http://www.sugarforge.org/projects/corporatestyle
 * 
 * Published on COSL SugarCRM OpenSource Library http://cosl.ithinkopen.com/sugarcrm
 * "Powered by think7Strategy.".
 ********************************************************************************/

*}

<div id="sideBar" class="{if $USE_GROUP_TABS}isGroupedModule{/if}">
    <div id="leftColumn">
            
            {* Shortcuts in leftColumn *}
            {* FG - Bug 41467 - Let Home module have Shortcuts *}
            {if count($SHORTCUT_MENU) > 0}
            <div id="shortcuts" class="leftList">
                <h3><span>{$APP.LBL_SHORTCUTS}</span></h3>
                <ul id="ul_shortcuts">
                {foreach from=$SHORTCUT_MENU item=item}
                <li style="white-space:nowrap;">
                    {if $item.URL == "-"}
                      <a></a><span>&nbsp;</span>
                    {else}
                      <a href="{$item.URL}"><span class="y">{$item.IMAGE}</span><span>{$item.LABEL}</span></a>
                    {/if}
                </li>
                {/foreach}
                </ul>
            </div>
            {/if}
            
            {* LastView in leftColumn *}
            <div id="lastView" class="leftList">
                <h3><span>{$APP.LBL_LAST_VIEWED}</span></h3>
                <ul id="ul_lastview">
                {foreach from=$recentRecords item=item name=lastViewed}
                <li style="white-space:nowrap;">
                    <a title="{$item.item_summary}"
                        accessKey="{$smarty.foreach.lastViewed.iteration}"
                        href="{sugar_link module=$item.module_name action='DetailView' record=$item.item_id link_only=1}">
                        <span class="y">{$item.image}</span><span>{$item.item_summary_short}</span>
                    </a>
                </li>
                {foreachelse}
                <li style="white-space:nowrap;">
                    {$APP.NTC_NO_ITEMS_DISPLAY}
                </li>
                {/foreach}
                </ul>
            </div>
            
            {* newRecord in leftColumn *}
            {* <div id="newRecord">
            </div> *}
            
    </div>
</div>
