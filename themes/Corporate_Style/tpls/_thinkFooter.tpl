{*
/*********************************************************************************
 * Corporate Style theme for SugarCRM
 * 
 * Community: http://www.ithinkopen.com
 * SugarCRM Chinese Forum: http://www.ithinkopen.com/sugarcrm
 * SugarForge: http://www.sugarforge.org/projects/corporatestyle
 * 
 * Published on COSL SugarCRM OpenSource Library http://cosl.ithinkopen.com/sugarcrm
 * "Powered by think7Strategy.".
 ********************************************************************************/

*}

<div id="thinkFooter">
	{php}
	    global $current_user, $current_language, $app_strings, $app_list_strings, $mod_strings;
	    $this->assign("thinkFooter", $app_strings);
	{/php}
	<a href="javascript:void(0)" onclick="SUGAR.util.top();" id="expandTopLink">Top</a>
	{*{foreach from=$thinkFooterMenus item=item name=footerMenu}*}
	<ul class="thinkFooterMenu">
		<li class="item-152"><a href="http://www.think7Strategy.com" rel="nofollow" target="_blank">{$thinkFooter.LBL_FOOTERMENU_WEBSITE}</a></li>
		<li class="item-152"><a href="http://www.ithinkopen.com/" rel="nofollow" target="_blank">{$thinkFooter.LBL_FOOTERMENU_SUPPORT_COMMUNITY}</a></li>
		<li class="item-152">
			<a href="http://www.sugarcrm.com/" rel="nofollow" target="_blank">{$thinkFooter.LBL_FOOTERMENU_POWERED_BY}</a>
			<div class="infoExpand active">
				<div class="copyrightInfo">{$COPYRIGHT}</div>
			</div>
		</li>
		{if $AUTHENTICATED}
		<li class="item-152"><a href="index.php?module=Calendar&amp;action=index">{$thinkFooter.LBL_FOOTERMENU_CALENDAR}</a></li>
		<li class="item-152"><a href="index.php?module=Emails&amp;action=index">{$thinkFooter.LBL_FOOTERMENU_EMAILS}</a></li>
		<li class="item-152"><a href="index.php?module=Meetings&amp;action=index">{$thinkFooter.LBL_FOOTERMENU_MEETINGS}</a></li>
		<li class="item-152"><a href="index.php?module=Calls&amp;action=index">{$thinkFooter.LBL_FOOTERMENU_CALLS}</a></li>
		{/if}
	</ul>
	<div id="bottomWidgets">
		{if $AUTHENTICATED}
		<div id="bottomLinks">
			{$BOTTOMLINKS}
		</div>
		{/if}
		<div id="responseTime">
			{$STATISTICS}
		</div>
	</div>
	<p>
		Theme Design &copy; <a href="http://cosl.ithinkopen.com/sugarcrm" rel="nofollow" target="_blank" title="SugarCRM Theme">COSL SugarCRM Theme</a>
	</p>
	<div id="expandStyleArea" class="thinkFooterColorMenu"> 
		<a href="#" id="expandColor1">Style I</a> 
		<a href="#" id="expandColor2">Style II</a> 
		<a href="#" id="expandColor3">Style III</a> 
	</div>
</div>
<div id="footer" class="none"></div>
<div class="clear"></div>
