{*
/*********************************************************************************
 * Corporate Style theme for SugarCRM
 * 
 * Community: http://www.ithinkopen.com
 * SugarCRM Chinese Forum: http://www.ithinkopen.com/sugarcrm
 * SugarForge: http://www.sugarforge.org/projects/corporatestyle
 * 
 * Published on COSL SugarCRM OpenSource Library http://cosl.ithinkopen.com/sugarcrm
 * "Powered by think7Strategy.".
 ********************************************************************************/

*}

<div id="socialLinks">
<aside>
    <a target="_blank" title="Sina Weibo" href="http://www.weibo.com/ithinkopen" id="topSinaWeibo" rel="nofollow">Sina Weibo</a> 
    <a target="_blank" title="Tencent Weibo" href="http://t.qq.com/ithinkopen" id="topTencentWeibo">Tencent Weibo</a>
    <a target="_blank" title="Facebook" href="http://www.facebook.com/ithinkopen" id="topFacebook" rel="nofollow">Facebook</a> 
    <a target="_blank" title="Twitter" href="http://twitter.com/ithinkopen" id="topTwitter" rel="nofollow">Twitter</a> 
</aside>
<ul id="expandLinksExtraMenu" class="none">
    <li><a id="expand_employees_link" class="pre-menu" href='index.php?module=Employees&action=index&query=true'>{$APP.LBL_EMPLOYEES}</a></li>
    <li><a id="expand_employees_link" class="sec-menu" href='index.php?module=Home&action=About'>{$APP.LNK_ABOUT}</a></li>
</ul>
</div>
