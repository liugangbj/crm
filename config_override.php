<?php
/***CONFIGURATOR***/
$sugar_config['addAjaxBannedModules'][0] = 'Home';
$sugar_config['addAjaxBannedModules'][1] = 'Accounts';
$sugar_config['addAjaxBannedModules'][2] = 'Bugs';
$sugar_config['addAjaxBannedModules'][3] = 'Calls';
$sugar_config['addAjaxBannedModules'][4] = 'Cases';
$sugar_config['addAjaxBannedModules'][5] = 'Contacts';
$sugar_config['addAjaxBannedModules'][6] = 'Leads';
$sugar_config['addAjaxBannedModules'][7] = 'Meetings';
$sugar_config['addAjaxBannedModules'][8] = 'Notes';
$sugar_config['addAjaxBannedModules'][9] = 'Opportunities';
$sugar_config['addAjaxBannedModules'][10] = 'ProspectLists';
$sugar_config['addAjaxBannedModules'][11] = 'Prospects';
$sugar_config['addAjaxBannedModules'][12] = 'Tasks';
$sugar_config['list_max_entries_per_page'] = '15';
$sugar_config['verify_client_ip'] = false;
$sugar_config['default_module_favicon'] = false;
$sugar_config['dashlet_auto_refresh_min'] = '30';
$sugar_config['enable_action_menu'] = true;
$sugar_config['stack_trace_errors'] = false;
$sugar_config['developerMode'] = true;
$sugar_config['default_theme'] = 'Corporate_Style';
$sugar_config['disabled_themes'] = '';
$sugar_config['disabled_languages'] = '';
$sugar_config['email_xss'] = 'YToxMzp7czo2OiJhcHBsZXQiO3M6NjoiYXBwbGV0IjtzOjQ6ImJhc2UiO3M6NDoiYmFzZSI7czo1OiJlbWJlZCI7czo1OiJlbWJlZCI7czo0OiJmb3JtIjtzOjQ6ImZvcm0iO3M6NToiZnJhbWUiO3M6NToiZnJhbWUiO3M6ODoiZnJhbWVzZXQiO3M6ODoiZnJhbWVzZXQiO3M6NjoiaWZyYW1lIjtzOjY6ImlmcmFtZSI7czo2OiJpbXBvcnQiO3M6ODoiXD9pbXBvcnQiO3M6NToibGF5ZXIiO3M6NToibGF5ZXIiO3M6NDoibGluayI7czo0OiJsaW5rIjtzOjY6Im9iamVjdCI7czo2OiJvYmplY3QiO3M6MzoieG1wIjtzOjM6InhtcCI7czo2OiJzY3JpcHQiO3M6Njoic2NyaXB0Ijt9';
/***CONFIGURATOR***/