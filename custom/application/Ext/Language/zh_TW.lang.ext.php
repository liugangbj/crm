<?php 
 //WARNING: The contents of this file are auto-generated


/*********************************************************************************
 * Corporate Style theme for SugarCRM
 * 
 * Community: http://www.ithinkopen.com
 * SugarCRM Chinese Forum: http://www.ithinkopen.com/sugarcrm
 * SugarForge: http://www.sugarforge.org/projects/corporatestyle
 * 
 * Published on COSL SugarCRM OpenSource Library http://cosl.ithinkopen.com/sugarcrm
 * "Powered by think7Strategy.".
 ********************************************************************************/


$app_strings['LBL_SUGAR_HOME'] = "Sugar 首頁";
$app_strings['LBL_FOOTERMENU_WEBSITE'] = "您的官網";
$app_strings['LBL_FOOTERMENU_SUPPORT_COMMUNITY'] = "ITO 開源社區";
$app_strings['LBL_FOOTERMENU_POWERED_BY'] = "Powered by SugarCRM";
$app_strings['LBL_FOOTERMENU_CALENDAR'] = "日歷";
$app_strings['LBL_FOOTERMENU_EMAILS'] = "Email";
$app_strings['LBL_FOOTERMENU_MEETINGS'] = "會議";
$app_strings['LBL_FOOTERMENU_CALLS'] = "電話";


?>