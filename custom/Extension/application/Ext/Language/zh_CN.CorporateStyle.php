<?php
/*********************************************************************************
 * Corporate Style theme for SugarCRM
 * 
 * Community: http://www.ithinkopen.com
 * SugarCRM Chinese Forum: http://www.ithinkopen.com/sugarcrm
 * SugarForge: http://www.sugarforge.org/projects/corporatestyle
 * 
 * Published on COSL SugarCRM OpenSource Library http://cosl.ithinkopen.com/sugarcrm
 * "Powered by think7Strategy.".
 ********************************************************************************/


$app_strings['LBL_SUGAR_HOME'] = "Sugar 首页";
$app_strings['LBL_FOOTERMENU_WEBSITE'] = "您的官网";
$app_strings['LBL_FOOTERMENU_SUPPORT_COMMUNITY'] = "ITO 开源社区";
$app_strings['LBL_FOOTERMENU_POWERED_BY'] = "Powered by SugarCRM";
$app_strings['LBL_FOOTERMENU_CALENDAR'] = "日历";
$app_strings['LBL_FOOTERMENU_EMAILS'] = "Email";
$app_strings['LBL_FOOTERMENU_MEETINGS'] = "会议";
$app_strings['LBL_FOOTERMENU_CALLS'] = "电话";

?>