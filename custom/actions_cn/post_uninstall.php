<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

stream_copy("themes/Sugar5/tpls/_headerModuleList_en_us.tpl", "themes/Sugar5/tpls/_headerModuleList.tpl");

function stream_copy($src, $dest) 
{ 
	$fsrc = fopen($src,'r'); 
	$fdest = fopen($dest,'w+'); 
	$len = stream_copy_to_stream($fsrc,$fdest); 
	fclose($fsrc); 
	fclose($fdest); 
	return $len; 
} 

?>
