<?php
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

$mod_strings = array (
  'LBL_TEAM' => '团队',
  'LBL_TEAM_ID' => '团队编号',
  'LBL_ASSIGNED_TO_ID' => '指派用户编号',
  'LBL_ASSIGNED_TO_NAME' => '指派给',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '创建日期',
  'LBL_DATE_MODIFIED' => '修改日期',
  'LBL_MODIFIED' => '修改人',
  'LBL_MODIFIED_ID' => '修改人ID',
  'LBL_MODIFIED_NAME' => '修改人名称',
  'LBL_CREATED' => '创建者',
  'LBL_CREATED_ID' => '创建者ID',
  'LBL_DESCRIPTION' => '说明',
  'LBL_DELETED' => '删除',
  'LBL_NAME' => '名称',
  'LBL_SAVING' => '保存...',
  'LBL_SAVED' => '保存',
  'LBL_CREATED_USER' => '创建用户',
  'LBL_MODIFIED_USER' => '用户修改',
  'LBL_LIST_FORM_TITLE' => 'Feed 列表',
  'LBL_MODULE_NAME' => '活动名称',
  'LBL_MODULE_TITLE' => '活动流',
  'LBL_DASHLET_DISABLED' => '警告: Feed 系统已禁止登陆, 直到它被激活将不会有新的条目公布',
  'LBL_ADMIN_SETTINGS' => 'Feed 设置',
  'LBL_RECORDS_DELETED' => '所有先前的Sugar Feed已被删除，如果系统被启用，新的Sugar Feed将被自动生成。',
  'LBL_CONFIRM_DELETE_RECORDS' => '您确定删除所有的Sugar Feed吗?',
  'LBL_FLUSH_RECORDS' => '删除 Feed 记录',
  'LBL_ENABLE_FEED' => '启用我的活动流的仪表盘',
  'LBL_ENABLE_MODULE_LIST' => '启用Sugar Feed',
  'LBL_HOMEPAGE_TITLE' => '我的活动流',
  'LNK_NEW_RECORD' => '创建 Feed',
  'LNK_LIST' => '源',
  'LBL_SEARCH_FORM_TITLE' => '查找 Feed',
  'LBL_HISTORY_SUBPANEL_TITLE' => '查看历史活动',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => '活动',
  'LBL_SUGAR_FEED_SUBPANEL_TITLE' => 'Feed',
  'LBL_NEW_FORM_TITLE' => '新 Feed',
  'LBL_ALL' => '所有',
  'LBL_USER_FEED' => '用户Feed',
  'LBL_ENABLE_USER_FEED' => '启用用户 Feed',
  'LBL_TO' => '团队可见',
  'LBL_IS' => '是',
  'LBL_DONE' => '完成',
  'LBL_TITLE' => '职称',
  'LBL_ROWS' => '行数',
  'LBL_CATEGORIES' => '模块',
  'LBL_TIME_LAST_WEEK' => '上周',
  'LBL_TIME_WEEKS' => '周',
  'LBL_TIME_DAYS' => '天',
  'LBL_TIME_YESTERDAY' => '昨天',
  'LBL_TIME_HOURS' => '小时',
  'LBL_TIME_HOUR' => '小时',
  'LBL_TIME_MINUTES' => '分钟',
  'LBL_TIME_MINUTE' => '分钟',
  'LBL_TIME_SECONDS' => '秒',
  'LBL_TIME_SECOND' => '秒',
  'LBL_TIME_AGO' => '之前',

  'CREATED_CONTACT' => '创建 <b>新</b>',
  'CREATED_OPPORTUNITY' => '创建一个 <b>新</b>',
  'CREATED_CASE' => '创建一个 <b>新</b>',
  'CREATED_LEAD' => '创建一个 <b>新</b>',
  'FOR' => '为',
  'CLOSED_CASE' => '<b>关闭</b> 一个 {0}',
  'CONVERTED_LEAD' => '<b>转换</b>一个潜在销售线索',
  'WON_OPPORTUNITY' => '已 <b>赢得</b> 一个 {0}',
  'WITH' => '和',

  'LBL_LINK_TYPE_Link' => '链接',
  'LBL_LINK_TYPE_Image' => '图片',
  'LBL_LINK_TYPE_YouTube' => 'YouTube&#153;',
  
  'LBL_SELECT' => '选择',
  'LBL_POST' => '发送',
  'LBL_EXTERNAL_PREFIX' => '外部:',
  'LBL_EXTERNAL_WARNING' => '项目标签 "外部" 需要一个<a href="?=EAPM">外部客户模块</a>.',
  'LBL_AUTHENTICATE' => '连接至',
  'LBL_AUTHENTICATION_PENDING' => '不是所有的您已选择的外部客户已经被认证。 点击 \'取消\' 返回澳选项窗口去认证外部客户，或者是点击 \'确认\' 不认证继续进行。',
  'LBL_ADVANCED_SEARCH' => '高级搜索',
  'LBL_BASICSEARCH' => '基础搜索',
  'LBL_SHOW_MORE_OPTIONS' => '显示更多选项',
  'LBL_HIDE_OPTIONS' => '隐藏选项',
  'LBL_VIEW' => '查看',
  'LBL_POST_TITLE' => '发布状态更新为',
  'LBL_URL_LINK_TITLE' => 'URL 使用链接',
  'LBL_TEAM_VISIBILITY_TITLE' => '可看见该发布的团队',
);
?>
