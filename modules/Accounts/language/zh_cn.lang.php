<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  Defines the English language pack for the base application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

$mod_strings = array (
	// DON'T CONVERT THESE THEY ARE MAPPINGS
	'db_name' => 'LBL_LIST_ACCOUNT_NAME',
	'db_website' => 'LBL_LIST_WEBSITE',
	'db_billing_address_city' => 'LBL_LIST_CITY',
	// END DON'T CONVERT
  'LBL_DOCUMENTS_SUBPANEL_TITLE' => '文档',
	// Dashlet Categories
  'LBL_CHARTS' => '图表',
  'LBL_DEFAULT' => '视图',
  'LBL_MISC' => '杂项',
  'LBL_UTILS' => '工具',
	// END Dashlet Categories

  'ACCOUNT_REMOVE_PROJECT_CONFIRM' => '您确定要从此项目中移除这个账户吗？',
  'ERR_DELETE_RECORD' => '必须指定记录编号才能删除客户。',
  'LBL_ACCOUNT_INFORMATION' => '账户概述',
  'LBL_ACCOUNT_NAME' => '账户名称：',
  'LBL_ACCOUNT' => '账户：',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => '活动',
  'LBL_ADDRESS_INFORMATION' => '地址信息',
  'LBL_ANNUAL_REVENUE' => '年营业额：',
  'LBL_ANY_ADDRESS' => '任意地址：',
  'LBL_ANY_EMAIL' => '任意邮件：',
  'LBL_ANY_PHONE' => '任意电话：',
  'LBL_ASSIGNED_TO_NAME' => '负责人：',
  'LBL_ASSIGNED_TO_ID' => '负责人ID：',
  'LBL_BILLING_ADDRESS_CITY' => '账单城市：',
  'LBL_BILLING_ADDRESS_COUNTRY' => '账单国家：',
  'LBL_BILLING_ADDRESS_POSTALCODE' => '账单邮编：',
  'LBL_BILLING_ADDRESS_STATE' => '账单省：',
  'LBL_BILLING_ADDRESS_STREET_2' => '账单地址 2',
  'LBL_BILLING_ADDRESS_STREET_3' => '账单地址 3',
  'LBL_BILLING_ADDRESS_STREET_4' => '账单地址 4',
  'LBL_BILLING_ADDRESS_STREET' => '账单地址：',
  'LBL_BILLING_ADDRESS' => '账单地址：',
  'LBL_BUG_FORM_TITLE' => '账户',
  'LBL_BUGS_SUBPANEL_TITLE' => '缺陷',
  'LBL_CALLS_SUBPANEL_TITLE' => '电话',
  'LBL_CAMPAIGN_ID' => '商业机会编号',
  'LBL_CASES_SUBPANEL_TITLE' => '客户反馈',
  'LBL_CITY' => '城市：',
  'LBL_CONTACTS_SUBPANEL_TITLE' => '联系人',
  'LBL_COUNTRY' => '国家：',
  'LBL_DATE_ENTERED' => '创建日期：',
  'LBL_DATE_MODIFIED' => '修改日期',
  'LBL_MODIFIED_ID' => '修改人ID',
  'LBL_DEFAULT_SUBPANEL_TITLE' => '账户',
  'LBL_DESCRIPTION_INFORMATION' => '描述信息',
  'LBL_DESCRIPTION' => '描述：',
  'LBL_DUPLICATE' => '可能有重复账户',
  'LBL_EMAIL' => '电子邮件地址：',
  'LBL_EMAIL_OPT_OUT' => '退出邮件列表：',
  'LBL_EMAIL_ADDRESSES' => '邮件地址',
  'LBL_EMPLOYEES' => '员工：',
  'LBL_FAX' => '传真：',
  'LBL_HISTORY_SUBPANEL_TITLE' => '历史记录',
  'LBL_HOMEPAGE_TITLE' => '我的客户',
  'LBL_INDUSTRY' => '行业：',
  'LBL_INVALID_EMAIL' => '有效邮件：',
  'LBL_INVITEE' => '联系人',
  'LBL_LEADS_SUBPANEL_TITLE' => '潜在客户',
  'LBL_LIST_ACCOUNT_NAME' => '账户名称',
  'LBL_LIST_CITY' => '城市',
  'LBL_LIST_CONTACT_NAME' => '联系人姓名',
  'LBL_LIST_EMAIL_ADDRESS' => '邮件地址',
  'LBL_LIST_FORM_TITLE' => '客户列表',
  'LBL_LIST_PHONE' => '电话',
  'LBL_LIST_STATE' => '省',
  'LBL_LIST_WEBSITE' => '网站',
  'LBL_MEETINGS_SUBPANEL_TITLE' => '会议',
  'LBL_MEMBER_OF' => '父级组织：',
  'LBL_MEMBER_ORG_FORM_TITLE' => '子公司',
  'LBL_MEMBER_ORG_SUBPANEL_TITLE' => '子公司',
  'LBL_MODULE_NAME' => '客户',
  'LBL_MODULE_TITLE' => '客户：主页',
  'LBL_MODULE_ID' => '客户',
  'LBL_NAME' => '名称：',
  'LBL_NEW_FORM_TITLE' => '新账户',
  'LBL_OPPORTUNITIES_SUBPANEL_TITLE' => '商业机会',
  'LBL_OTHER_EMAIL_ADDRESS' => '其他邮件地址：',
  'LBL_OTHER_PHONE' => '其他电话：',
  'LBL_OWNERSHIP' => '所有者：',
  'LBL_PARENT_ACCOUNT_ID' => '父账户ID',
  'LBL_PHONE_ALT' => '备用电话：',
  'LBL_PHONE_FAX' => '传真：',
  'LBL_PHONE_OFFICE' => '办公室电话：',
  'LBL_PHONE' => '电话：',
  'LBL_POSTAL_CODE' => '邮编：',
  'LBL_PRODUCTS_TITLE' => '产品',
  'LBL_PROJECTS_SUBPANEL_TITLE' => '项目',
  'LBL_PUSH_BILLING' => '推送账单',
  'LBL_PUSH_CONTACTS_BUTTON_LABEL' => '复制至联系人',
  'LBL_PUSH_CONTACTS_BUTTON_TITLE' => '复制...',
  'LBL_PUSH_SHIPPING' => '推送货运',
  'LBL_RATING' => '评分：',
  'LBL_SAVE_ACCOUNT' => '保存账户',
  'LBL_SEARCH_FORM_TITLE' => '客户搜索',
  'LBL_SHIPPING_ADDRESS_CITY' => '货运城市：',
  'LBL_SHIPPING_ADDRESS_COUNTRY' => '货运国家：',
  'LBL_SHIPPING_ADDRESS_POSTALCODE' => '货运邮编：',
  'LBL_SHIPPING_ADDRESS_STATE' => '货运省份：',
  'LBL_SHIPPING_ADDRESS_STREET_2' => '货运街道 2',
  'LBL_SHIPPING_ADDRESS_STREET_3' => '货运街道 3',
  'LBL_SHIPPING_ADDRESS_STREET_4' => '货运街道 4',
  'LBL_SHIPPING_ADDRESS_STREET' => '货运街道：',
  'LBL_SHIPPING_ADDRESS' => '货运地址：',
  'LBL_SIC_CODE' => 'SIC代码：',
  'LBL_STATE' => '省：',
  'LBL_TASKS_SUBPANEL_TITLE' => '任务',
  'LBL_TEAMS_LINK' => '团队',
  'LBL_TICKER_SYMBOL' => '股票代码:',
  'LBL_TYPE' => '类型：',
  'LBL_USERS_ASSIGNED_LINK' => '负责人',
  'LBL_USERS_CREATED_LINK' => '创建人',
  'LBL_USERS_MODIFIED_LINK' => '修改人',
  'LBL_VIEW_FORM_TITLE' => '客户视图',
  'LBL_WEBSITE' => '网站：',
  'LBL_CREATED_ID' => '创建人ID：',
  'LBL_CAMPAIGNS' => '市场活动',
  'LNK_ACCOUNT_LIST' => '查看客户',
  'LNK_NEW_ACCOUNT' => '创建客户',
  'LNK_IMPORT_ACCOUNTS' => '导入客户',
  'MSG_DUPLICATE' => '您正要创建的这个客户也许存在重复记录。下面列举出了包含类似名称的客户。<br>点击创建客户来继续创建新的客户，或选择一个下面已经存在的客户。',
  'MSG_SHOW_DUPLICATES' => '您正要创建的这个客户也许存在重复记录。下面列举出了包含类似名称的客户。<br>点击保存来创建这个客户，或点击取消来返回之前的模块。',
  'NTC_COPY_BILLING_ADDRESS' => '复制账单地址至货运地址',
  'NTC_COPY_BILLING_ADDRESS2' => '复制至货运地址',
  'NTC_COPY_SHIPPING_ADDRESS' => '复制货运地址至账单地址',
  'NTC_COPY_SHIPPING_ADDRESS2' => '复制至账单地址',
  'NTC_DELETE_CONFIRMATION' => '您确定要删除这条记录吗？',
  'NTC_REMOVE_ACCOUNT_CONFIRMATION' => '您确定要移除这条记录吗？',
  'NTC_REMOVE_MEMBER_ORG_CONFIRMATION' => '您确定要删除这条子公司记录吗？',
  'LBL_ASSIGNED_USER_NAME' => '负责人：',
  'LBL_PROSPECT_LIST' => '目标列表',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => '客户',
  'LBL_PROJECT_SUBPANEL_TITLE' => '项目',
  'LBL_COPY' => '复制',
    //For export labels
  'LBL_ACCOUNT_TYPE' => '账户类型',
  'LBL_CAMPAIGN_ID' => '商业机会编号',
  'LBL_PARENT_ID' => '父ID',
  'LBL_PHONE_ALTERNATE' => '替代电话',
  'LBL_EXPORT_ASSIGNED_USER_NAME' => '负责人用户名',
    // SNIP
  'LBL_CONTACT_HISTORY_SUBPANEL_TITLE' => '关联 联系人\' Emails',
);
?>
