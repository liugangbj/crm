<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  Defines the English language pack for the base application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

$mod_strings = array (
  'LBL_LIST_ID' => '潜在客户列表编号',
  'LBL_ID' => 'ID',
  'LBL_TARGET_TRACKER_KEY' => '目标跟踪器索引',
  'LBL_TARGET_ID' => '目标ID',
  'LBL_TARGET_TYPE' => '目标类型',
  'LBL_ACTIVITY_TYPE' => '活动类型',
  'LBL_ACTIVITY_DATE' => '活动日期',
  'LBL_RELATED_ID' => '相关编号',
  'LBL_RELATED_TYPE' => '相关类型',
  'LBL_DELETED' => '已删除',
  'LBL_MODULE_NAME' => '营销活动日志',
  'LBL_LIST_RECIPIENT_EMAIL' => '收件人邮箱',
  'LBL_LIST_RECIPIENT_NAME' => '收件人名称',
  'LBL_ARCHIVED' => '已存档',
  'LBL_HITS' => '点击',
	
  'LBL_CAMPAIGN_NAME' => '名称:',
  'LBL_CAMPAIGN' => '市场活动:',
  'LBL_NAME' => '名称:',
  'LBL_INVITEE' => '联系人',
  'LBL_LIST_CAMPAIGN_NAME' => '市场活动',
  'LBL_LIST_STATUS' => '状态',
  'LBL_LIST_TYPE' => '类型',
  'LBL_LIST_END_DATE' => '结束日期',
  'LBL_DATE_ENTERED' => '日期已输入',
  'LBL_DATE_MODIFIED' => '日期已更改',
  'LBL_MODIFIED' => '更改人:',
  'LBL_CREATED' => '创建人:',
  'LBL_TEAM' => '团队:',
  'LBL_ASSIGNED_TO' => '负责人:',
  'LBL_CAMPAIGN_START_DATE' => '开始日期:',
  'LBL_CAMPAIGN_END_DATE' => '结束日期:',
  'LBL_CAMPAIGN_STATUS' => '状态:',
  'LBL_CAMPAIGN_BUDGET' => '预算:',
  'LBL_CAMPAIGN_EXPECTED_COST' => '预期成本:',
  'LBL_CAMPAIGN_ACTUAL_COST' => '实际成本:',
  'LBL_CAMPAIGN_EXPECTED_REVENUE' => '预期收入:',
  'LBL_CAMPAIGN_TYPE' => '类型:',
  'LBL_CAMPAIGN_OBJECTIVE' => '目标:',
  'LBL_CAMPAIGN_CONTENT' => '说明:',
  'LBL_CREATED_LEAD' => '已创建潜在客户',
  'LBL_CREATED_CONTACT' => '已创建客户',
  'LBL_CREATED_OPPORTUNITY' => '新增商业机会',
  'LBL_TARGETED_USER' => '目标用户',
  'LBL_SENT_EMAIL' => '已发邮件',
  'LBL_LIST_FORM_TITLE' => '目标市场活动',
  'LBL_LIST_ACTIVITY_DATE' => '市场活动日期',
  'LBL_LIST_CAMPAIGN_OBJECTIVE' => '市场活动目标',
  'LBL_RELATED' => '关联',
  'LBL_CLICKED_URL_KEY' => '点击网址索引',
  'LBL_URL_CLICKED' => '网址点击',
  'LBL_MORE_INFO' => '更多信息',
    
  'LBL_CAMPAIGNS' => '市场活动',
  'LBL_LIST_MARKETING_NAME' => '市场活动ID',
);
?>
