<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  Defines the English language pack for the base application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

$mod_strings = array (
  'LBL_MODULE_NAME' => '目标',
  'LBL_MODULE_ID' => '目标',
  'LBL_INVITEE' => '直接报告',
  'LBL_MODULE_TITLE' => '目标: 主页',
  'LBL_SEARCH_FORM_TITLE' => '目标查找',
  'LBL_LIST_FORM_TITLE' => '目标列表',
  'LBL_NEW_FORM_TITLE' => '新增目标',
  'LBL_PROSPECT' => '目标:',
  'LBL_BUSINESSCARD' => '商务名片',
  'LBL_LIST_NAME' => '名称',
  'LBL_LIST_LAST_NAME' => '姓',
  'LBL_LIST_PROSPECT_NAME' => '目标姓名',
  'LBL_LIST_TITLE' => '职称',
  'LBL_LIST_EMAIL_ADDRESS' => '电子邮件',
  'LBL_LIST_OTHER_EMAIL_ADDRESS' => '其他电子邮件',
  'LBL_LIST_PHONE' => '电话',
  'LBL_LIST_PROSPECT_ROLE' => '角色',
  'LBL_LIST_FIRST_NAME' => '名',
  'LBL_ASSIGNED_TO_NAME' => '负责人',
  'LBL_ASSIGNED_TO_ID' => '负责人：',
//DON'T CONVERT THESE THEY ARE MAPPINGS
  'db_last_name' => 'LBL_LIST_LAST_NAME',
  'db_first_name' => 'LBL_LIST_FIRST_NAME',
  'db_title' => 'LBL_LIST_TITLE',
  'db_email1' => 'LBL_LIST_EMAIL_ADDRESS',
  'db_email2' => 'LBL_LIST_OTHER_EMAIL_ADDRESS',
//END DON'T CONVERT
  'LBL_CAMPAIGN_ID' => '市场活动ID',
  'LBL_EXISTING_PROSPECT' => '使用一个已经存在的联系人',
  'LBL_CREATED_PROSPECT' => '创建新联系人',
  'LBL_EXISTING_ACCOUNT' => '使用一个现有客户',
  'LBL_CREATED_ACCOUNT' => '创建新客户',
  'LBL_CREATED_CALL' => '创建新电话',
  'LBL_CREATED_MEETING' => '创建新会议',
  'LBL_ADDMORE_BUSINESSCARD' => '增加更多的商务名片',
  'LBL_ADD_BUSINESSCARD' => '输入商务名片',
  'LBL_NAME' => '名称:',
  'LBL_FULL_NAME' => '名称',
  'LBL_PROSPECT_NAME' => '目标名称:',
  'LBL_PROSPECT_INFORMATION' => '目标信息',
  'LBL_MORE_INFORMATION' => '更多信息',
  'LBL_FIRST_NAME' => '名:',
  'LBL_OFFICE_PHONE' => '办公室电话:',
  'LBL_ANY_PHONE' => '任何电话:',
  'LBL_PHONE' => '电话:',
  'LBL_LAST_NAME' => '姓:',
  'LBL_MOBILE_PHONE' => '移动电话:',
  'LBL_HOME_PHONE' => '家庭电话:',
  'LBL_OTHER_PHONE' => '其他电话:',
  'LBL_FAX_PHONE' => '传真:',
  'LBL_PRIMARY_ADDRESS_STREET' => '主要住址街道:',
  'LBL_PRIMARY_ADDRESS_CITY' => '主要住址城市:',
  'LBL_PRIMARY_ADDRESS_COUNTRY' => '主要住址国家:',
  'LBL_PRIMARY_ADDRESS_STATE' => '主要住址省份:',
  'LBL_PRIMARY_ADDRESS_POSTALCODE' => '主要住址邮政编码:',
  'LBL_ALT_ADDRESS_STREET' => '其他地址街道:',
  'LBL_ALT_ADDRESS_CITY' => '其他地址城市:',
  'LBL_ALT_ADDRESS_COUNTRY' => '其他地址国家:',
  'LBL_ALT_ADDRESS_STATE' => '其他地址省份:',
  'LBL_ALT_ADDRESS_POSTALCODE' => '其他地址邮政编码:',
  'LBL_TITLE' => '职称:',
  'LBL_DEPARTMENT' => '部门:',
  'LBL_BIRTHDATE' => '生日:',
  'LBL_EMAIL_ADDRESS' => '电子邮件地址:',
  'LBL_OTHER_EMAIL_ADDRESS' => '其他邮件:',
  'LBL_ANY_EMAIL' => '任何邮件:',
  'LBL_ASSISTANT' => '助理:',
  'LBL_ASSISTANT_PHONE' => '助理电话:',
  'LBL_DO_NOT_CALL' => '谢绝来电:',
  'LBL_EMAIL_OPT_OUT' => '退出邮件',
  'LBL_PRIMARY_ADDRESS' => '主要地址:',
  'LBL_ALTERNATE_ADDRESS' => '其他地址:',
  'LBL_ANY_ADDRESS' => '任何地址:',
  'LBL_CITY' => '城市:',
  'LBL_STATE' => '省份:',
  'LBL_POSTAL_CODE' => '邮政编码:',
  'LBL_COUNTRY' => '国家:',
  'LBL_DESCRIPTION_INFORMATION' => '说明信息',
  'LBL_ADDRESS_INFORMATION' => '地址信息',
  'LBL_DESCRIPTION' => '说明:',
  'LBL_PROSPECT_ROLE' => '角色:',
  'LBL_OPP_NAME' => '商业机会名称:',
  'LBL_IMPORT_VCARD' => '导入vCard',
  'LBL_IMPORT_VCARDTEXT' => '从您的文件系统里通过导入vCard自动生成一个联系人。',
  'LBL_DUPLICATE' => '可能重复的目标',
  'MSG_SHOW_DUPLICATES' => '您想要创建的目标记录可能造成已存在的目标记录的重复。下面列出的是包括相同名称和邮件地址的目标记录。<br>点击创建目标继续创建新目标，或者在下面已有目标中选择一个。',
  'MSG_DUPLICATE' => '您想要创建的目标记录可能造成已存在的目标记录的重复。下面列出的是包括相同名称和邮件地址的目标记录。<br>点击保存继续创建此新目标，或者点击取消返回模块不创建目标。',
  'LNK_IMPORT_VCARD' => '从vCard新增',
  'LNK_NEW_ACCOUNT' => '创建客户',
  'LNK_NEW_OPPORTUNITY' => '创建商业机会',
  'LNK_NEW_CASE' => '创建客户反馈',
  'LNK_NEW_NOTE' => '创建记录和附件',
  'LNK_NEW_CALL' => '电话记录',
  'LNK_NEW_EMAIL' => '存档电子邮件',
  'LNK_NEW_MEETING' => '安排会议',
  'LNK_NEW_TASK' => '创建任务',
  'LNK_NEW_APPOINTMENT' => '创建约会',
  'LNK_IMPORT_PROSPECTS' => '导入潜在客户',
  'NTC_DELETE_CONFIRMATION' => '您确定想要删除此记录吗?',
  'NTC_REMOVE_CONFIRMATION' => '您确定想要将此联系人从此案例里中移除吗?',
  'NTC_REMOVE_DIRECT_REPORT_CONFIRMATION' => '您确定将此直接报告人的记录移除吗?',
  'ERR_DELETE_RECORD' => '您必须指定一个记录编号才可删除此联系人。',
  'NTC_COPY_PRIMARY_ADDRESS' => '复制此主要地址到替换地址。',
  'NTC_COPY_ALTERNATE_ADDRESS' => '复制此替换地址到主要地址。',
  'LBL_SALUTATION' => '称谓',
  'LBL_SAVE_PROSPECT' => '保存目标',
  'LBL_CREATED_OPPORTUNITY' => '创建一个新的商业机会',
  'NTC_OPPORTUNITY_REQUIRES_ACCOUNT' => '创建新的商业机会需要客户\\n 您可以创建一个新的客户或在已有客户中选择。.',
  'LNK_SELECT_ACCOUNT' => '选择客户',
  'LNK_NEW_PROSPECT' => '创建目标',
  'LNK_PROSPECT_LIST' => '查看目标',
  'LNK_NEW_CAMPAIGN' => '创建市场活动',
  'LNK_CAMPAIGN_LIST' => '市场活动',
  'LNK_NEW_PROSPECT_LIST' => '创建目标列表',
  'LNK_PROSPECT_LIST_LIST' => '目标列表',
  'LNK_IMPORT_PROSPECT' => '导入目标列表',
  'LBL_SELECT_CHECKED_BUTTON_LABEL' => '选择选中的目标',
  'LBL_SELECT_CHECKED_BUTTON_TITLE' => '选择选中的目标',
  'LBL_INVALID_EMAIL' => '无效的电子邮件:',
  'LBL_DEFAULT_SUBPANEL_TITLE' => '目标',
  'LBL_PROSPECT_LIST' => '潜在客户列表',
  'LBL_CONVERT_BUTTON_KEY' => 'V',
  'LBL_CONVERT_BUTTON_TITLE' => '转换目标',
  'LBL_CONVERT_BUTTON_LABEL' => '转换目标',
  'LBL_CONVERTPROSPECT' => '转换目标',
  'LNK_NEW_CONTACT' => '新联系人',
  'LBL_CREATED_CONTACT' => '创建一个新联系人',
  'LBL_BACKTO_PROSPECTS' => '返回目标',
  'LBL_CAMPAIGNS' => '市场活动',
  'LBL_CAMPAIGN_LIST_SUBPANEL_TITLE' => '市场活动日志',
  'LBL_TRACKER_KEY' => '目标索引',
  'LBL_LEAD_ID' => '潜在用户编号',
  'LBL_CONVERTED_LEAD' => '转换潜在用户',
  'LBL_ACCOUNT_NAME' => '客户名称',
  'LBL_EDIT_ACCOUNT_NAME' => '客户名称:',
  'LBL_CREATED_USER' => '创建新用户',
  'LBL_MODIFIED_USER' => '更改人',
  'LBL_CAMPAIGNS_SUBPANEL_TITLE' => '市场活动',
  'LBL_HISTORY_SUBPANEL_TITLE' => '历史活动',
  //For export labels
  'LBL_PHONE_HOME' => '电话 首页',
  'LBL_PHONE_MOBILE' => '移动电话',
  'LBL_PHONE_WORK' => '工作电话',
  'LBL_PHONE_OTHER' => '其它电话',
  'LBL_PHONE_FAX' => '电话 传真',
  'LBL_CAMPAIGN_ID' => '市场活动ID',
  'LBL_EXPORT_ASSIGNED_USER_NAME' => '被分配者名称',
  'LBL_EXPORT_ASSIGNED_USER_ID' => '被分配用户',
  'LBL_EXPORT_MODIFIED_USER_ID' => '修改人ID',
  'LBL_EXPORT_CREATED_BY' => '创建人ID',
  'LBL_EXPORT_EMAIL2' => '其它电子邮件地址',
);
?>
