<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  Defines the English language pack for the base application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/


$mod_strings = array (
  'LNK_NEW_CALL' => '安排电话',
  'LNK_NEW_MEETING' => '安排电话',
  'LNK_NEW_TASK' => '新增任务',
  'LNK_NEW_NOTE' => '新增备忘录',
  'LNK_NEW_EMAIL' => '存档电子邮件',
  'LNK_CALL_LIST' => '电话',
  'LNK_MEETING_LIST' => '会议',
  'LNK_TASK_LIST' => '任务',
  'LNK_NOTE_LIST' => '备注',
  'LNK_EMAIL_LIST' => '电子邮件',
  'LBL_ADD_FIELD' => '添加字段:',
  'LBL_MODULE_SELECT' => '编辑的模块',
  'LBL_SEARCH_FORM_TITLE' => '模块查找',
  'COLUMN_TITLE_NAME' => '字段名',
  'COLUMN_TITLE_DISPLAY_LABEL' => '显示标签',
  'COLUMN_TITLE_LABEL_VALUE' => '标签值',
  'COLUMN_TITLE_LABEL' => '系统标签',
  'COLUMN_TITLE_DATA_TYPE' => '数据类型',
  'COLUMN_TITLE_MAX_SIZE' => '最大长度',
  'COLUMN_TITLE_HELP_TEXT' => '帮助文本',
  'COLUMN_TITLE_COMMENT_TEXT' => '注释文本',
  'COLUMN_TITLE_REQUIRED_OPTION' => '必填字段',
  'COLUMN_TITLE_DEFAULT_VALUE' => '默认值',
  'COLUMN_TITLE_DEFAULT_EMAIL' => '默认值',
  'COLUMN_TITLE_EXT1' => '扩展字段 1',
  'COLUMN_TITLE_EXT2' => '扩展字段 2',
  'COLUMN_TITLE_EXT3' => '扩展字段 3',
  'COLUMN_TITLE_FRAME_HEIGHT' => '浮动框架高度',
  'COLUMN_TITLE_HTML_CONTENT' => 'HTML',
  'COLUMN_TITLE_URL' => '默认URL',
  'COLUMN_TITLE_AUDIT' => '审核',
  'COLUMN_TITLE_FTS' => '可搜索的全文',
  'COLUMN_TITLE_REPORTABLE' => '可报告的',
  'COLUMN_TITLE_MIN_VALUE' => '最小值',
  'COLUMN_TITLE_MAX_VALUE' => '最大值',
  'COLUMN_TITLE_LABEL_ROWS' => '行数',
  'COLUMN_TITLE_LABEL_COLS' => '列数',
  'COLUMN_TITLE_DISPLAYED_ITEM_COUNT' => '# 显示的项',
  'COLUMN_TITLE_AUTOINC_NEXT' => '自动增长字段值',
  'COLUMN_DISABLE_NUMBER_FORMAT' => '禁用格式',
  'COLUMN_TITLE_ENABLE_RANGE_SEARCH' => '能够进行范围查询',
  'COLUMN_TITLE_GLOBAL_SEARCH' => '全局搜索',
  'LBL_DROP_DOWN_LIST' => '下拉式列表',
  'LBL_RADIO_FIELDS' => '单选字段',
  'LBL_MULTI_SELECT_LIST' => '多选列表',
  'COLUMN_TITLE_PRECISION' => '精确度',
  'MSG_DELETE_CONFIRM' => '您确定要删除这个条目吗？',
  'POPUP_INSERT_HEADER_TITLE' => '增加自定义字段',
  'POPUP_EDIT_HEADER_TITLE' => '编辑自定义字段',
  'LNK_SELECT_CUSTOM_FIELD' => '选择自定义字段',
  'LNK_REPAIR_CUSTOM_FIELD' => '修复自定义字段',
  'LBL_MODULE' => '模块',
  'COLUMN_TITLE_MASS_UPDATE' => '批量更新',
  'COLUMN_TITLE_IMPORTABLE' => '可导入的',
  'COLUMN_TITLE_DUPLICATE_MERGE' => '合并重复',
  'LBL_LABEL' => '标签',
  'LBL_DATA_TYPE' => '数据类型',
  'LBL_DEFAULT_VALUE' => '默认值',
  'LBL_AUDITED' => '审核',
  'LBL_REPORTABLE' => '可报告的',
  'ERR_RESERVED_FIELD_NAME' => '保留字',
  'ERR_SELECT_FIELD_TYPE' => '请选择字段类型',
  'ERR_FIELD_NAME_ALREADY_EXISTS' => '字段名已经存在',
  'LBL_BTN_ADD' => '添加',
  'LBL_BTN_EDIT' => '编辑',
  'LBL_GENERATE_URL' => '生成 URL',
  'LBL_DEPENDENT_CHECKBOX' => '选择字段',
  'LBL_DEPENDENT_TRIGGER' => '触发器',
  'LBL_CALCULATED' => '可计算值',
  'LBL_FORMULA' => '公式',
  'LBL_DYNAMIC_VALUES_CHECKBOX' => '选择字段值',
  'LBL_BTN_EDIT_VISIBILITY' => '编辑可见度',
  'LBL_LINK_TARGET' => '打开 Link In',
  'LBL_IMAGE_WIDTH' => '宽',
  'LBL_IMAGE_HEIGHT' => '高',
  'LBL_IMAGE_BORDER' => '边框',
  'COLUMN_TITLE_VALIDATE_US_FORMAT' => '美国公式',
  'LBL_DEPENDENT' => '选择字段',
  'LBL_VISIBLE_IF' => '是否可见',
  'LBL_ENFORCED' => '强制的',
  'LBL_HELP' => '帮助' /*for 508 compliance fix*/,
  'COLUMN_TITLE_GLOBAL_SEARCH' => '全局搜索',
);


?>
