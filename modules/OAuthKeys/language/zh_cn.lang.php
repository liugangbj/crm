<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


$mod_strings = array (
  'LBL_CONSKEY' => '用户密匙',
  'LBL_CONSSECRET' => '用户秘密',
  'LBL_ASSIGNED_TO_ID' => '负责人ID',
  'LBL_ASSIGNED_TO_NAME' => '用户',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '创建日期',
  'LBL_DATE_MODIFIED' => '修改日期',
  'LBL_MODIFIED' => '由修改',
  'LBL_MODIFIED_ID' => '由Id修改',
  'LBL_MODIFIED_NAME' => '以名称修改',
  'LBL_CREATED' => '由_创建',
  'LBL_CREATED_ID' => '由Id创建',
  'LBL_DESCRIPTION' => '描述',
  'LBL_DELETED' => '删除',
  'LBL_NAME' => '用户密匙名称',
  'LBL_CREATED_USER' => '由用户创建',
  'LBL_MODIFIED_USER' => '由用户修改',
  'LBL_LIST_NAME' => '密匙名称',
  'LBL_LIST_FORM_TITLE' => '授权密匙',
  'LBL_MODULE_NAME' => '授权密匙',
  'LBL_MODULE_TITLE' => '授权密匙',
  'LNK_NEW_RECORD' => '创建OAuth密匙',
  'LNK_LIST' => '查看OAuth 密匙',
  'LBL_TOKENS' => '标识',
);
